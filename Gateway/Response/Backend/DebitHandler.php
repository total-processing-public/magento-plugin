<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Response\Backend;

use Magento\Payment\Gateway\Response\HandlerInterface;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Gateway\SubjectReader;
use Magento\Backend\Model\Session\Quote as SessionQuote;

/**
 * Class DebitHandler
 * @package TotalProcessing\Opp\Gateway\Response\Backend
 */
class DebitHandler implements HandlerInterface
{
    const CHECKOUT_ID = 'id';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param SubjectReader $subjectReader
     * @param SessionQuote $sessionQuote
     */
    public function __construct(
        SubjectReader $subjectReader,
        SessionQuote $sessionQuote
    ) {
        $this->subjectReader = $subjectReader;
        $this->sessionQuote = $sessionQuote;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(array $handlingSubject, array $response)
    {
        $checkoutId = $this->subjectReader->readResponse($response, self::CHECKOUT_ID);
        if (!$checkoutId) {
            throw new \InvalidArgumentException('Checkout can\'t be initialized.');
        }
        $this->sessionQuote->setCheckoutId($checkoutId);
    }
}
