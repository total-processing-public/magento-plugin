<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Response;

use Magento\Framework\App\CacheInterface;
use TotalProcessing\Opp\Gateway\Request\CustomParameterDataBuilder;
use TotalProcessing\Opp\Gateway\SubjectReader;
use Magento\Payment\Gateway\Response\HandlerInterface;
use Magento\Framework\Serialize\Serializer\Json as Serializer;
use TotalProcessing\Opp\Api\WebhookOrderProcessorInterface;

/**
 * Class PaymentStatusTransactionDetailsHandler
 * @package TotalProcessing\Opp\Gateway\Response
 */
class PaymentStatusTransactionDetailsHandler implements HandlerInterface
{
    /**
     * Custom parameters of the request.
     */
    const CUSTOM_PARAMETERS = 'customParameters';

    /**
     * Payment status response cache key
     */
    const PAYMENT_STATUS_RESPONSE_CACHE_KEY = 'opp_payment_status_response_cache_key';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var WebhookOrderProcessorInterface
     */
    private $webhookOrderProcessor;

    /**
     * @param SubjectReader $subjectReader
     * @param CacheInterface $cache
     * @param Serializer $serializer
     * @param WebhookOrderProcessorInterface $webhookOrderProcessor
     */
    public function __construct(
        SubjectReader $subjectReader,
        CacheInterface $cache,
        Serializer $serializer,
        WebhookOrderProcessorInterface $webhookOrderProcessor
    ) {
        $this->subjectReader = $subjectReader;
        $this->cache = $cache;
        $this->serializer = $serializer;
        $this->webhookOrderProcessor = $webhookOrderProcessor;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(array $handlingSubject, array $response)
    {
        if ($paymentStatusResponse = $this->subjectReader->readResponse($response)) {
            $customParameters = $this->subjectReader->readResponse($response, self::CUSTOM_PARAMETERS);
            if (!empty($customParameters)) {
                $quoteId = $this->subjectReader->readResponse(
                    $customParameters,
                    CustomParameterDataBuilder::QUOTE_ID
                );
                $webhooksUpdateRequired = $this->subjectReader->readResponse(
                    $customParameters,
                    CustomParameterDataBuilder::WEBHOOKS_UPDATE_REQUIRED
                );
                if ($webhooksUpdateRequired) {
                    // further processing by webhook
                    return;
                }

                try {
                    $cacheKey = sprintf('%s_%d', self::PAYMENT_STATUS_RESPONSE_CACHE_KEY, (int)$quoteId);
                    $data = $this->serializer->serialize($paymentStatusResponse);
                    $this->cache->save($data, $cacheKey);
                } catch (\InvalidArgumentException $e) {
                    // omit exception
                }
            }
        }
    }
}
