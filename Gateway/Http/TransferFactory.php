<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Http;

use Laminas\Http\Request;
use Magento\Payment\Gateway\Http\TransferInterface;
use Magento\Payment\Gateway\Http\TransferFactoryInterface;
use TotalProcessing\Opp\Gateway\Request\BaseRequestDataBuilder;

/**
 * Class TransferFactory
 * @package TotalProcessing\Opp\Gateway\Http
 */
class TransferFactory implements TransferFactoryInterface
{
    const HEADER_KEY_AUTHORIZATION = 'Authorization';

    /**
     * @var TransferBuilder
     */
    protected $transferBuilder;

    /**
     * Constructor
     *
     * @param TransferBuilder $transferBuilder
     */
    public function __construct(
        TransferBuilder $transferBuilder
    ) {
        $this->transferBuilder = $transferBuilder;
    }

    /**
     * Format authorization headers to the correct valid format for payment compatibility.
     * Previously if the input headers data were in format:
     * [
     *       'header_key_1' => 'header_value_1',
     *       'header_key_2' => 'header_value_2'
     *       ...
     * ]
     * The output headers data was generated to the following format:
     * [
     *       0 => 'header_key_1: header_value_1',
     *       0 => 'header_key_2: header_value_2'
     *       ...
     * ]
     * Starting with version 2.4.6 Zend_HTTP replaced with laminas-http.
     * @see https://developer.adobe.com/commerce/php/development/backward-incompatible-changes/highlights/#zend_http-replaced-with-laminas-http
     * In this case the headers data are generated as is in format:
     * [
     *       'header_key_1' => 'header_value_1',
     *       'header_key_2' => 'header_value_2'
     *       ...
     * ]
     * Seems that for authorization purpose the payment gateway doesn't accept such format
     * and returns an authorization error. Need to adjust it for correct processing.
     * @param array $headers
     * @return void
     */
    private function prepareAuthorizationHeaders(array &$headers = []): void
    {
        if (
            array_key_exists(self::HEADER_KEY_AUTHORIZATION, $headers)
            && 'Bearer' === substr($headers[self::HEADER_KEY_AUTHORIZATION], 0, 6)
            && class_exists(\Magento\Framework\HTTP\LaminasClient::class)
        ) {
            $headers[self::HEADER_KEY_AUTHORIZATION] = sprintf('%s: %s',
                self::HEADER_KEY_AUTHORIZATION,
                $headers['Authorization']
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function create(array $request): TransferInterface
    {
        $headers = [];
        if (isset($request[BaseRequestDataBuilder::REQUEST_DATA_NAMESPACE])) {
            $requestData = $request[BaseRequestDataBuilder::REQUEST_DATA_NAMESPACE];
            $method = $requestData[BaseRequestDataBuilder::REQUEST_DATA_METHOD] ?? Request::METHOD_GET;
            $url = $requestData[BaseRequestDataBuilder::REQUEST_DATA_URL] ?? '';
            $headers = $requestData[BaseRequestDataBuilder::REQUEST_DATA_HEADERS] ?? [];
            $encode = $requestData[BaseRequestDataBuilder::REQUEST_ENCODE] ?? false;
            unset($request[BaseRequestDataBuilder::REQUEST_DATA_NAMESPACE]);
        }

        $this->prepareAuthorizationHeaders($headers);

        return $this->transferBuilder
            ->setUri($url ?? '')
            ->setMethod($method ?? Request::METHOD_GET)
            ->setHeaders($headers ?? [])
            ->setBody($request[BaseRequestDataBuilder::REQUEST_DATA_RAW_BODY] ?? $request)
            ->shouldEncode($encode ?? false)
            ->build();
    }
}
