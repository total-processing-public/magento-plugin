<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Helper\Backend;

use Magento\Backend\Model\Session\Quote as SessionQuote;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Payment\Gateway\Command\CommandManagerInterface;
use Magento\Payment\Gateway\CommandInterface;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Gateway\Command\Backend\PaymentStatusCommand;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Model\System\Config\PaymentAction;
use TotalProcessing\Opp\Gateway\Request\PaymentStatusRequestDataBuilder;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class PaymentStatusResolver
 * @package TotalProcessing\Opp\Gateway\Helper\Backend
 */
class PaymentStatusResolver
{
    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var CommandManagerInterface
     */
    private $commandManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param SessionQuote $sessionQuote
     * @param RequestInterface $request
     * @param CommandManagerInterface $commandManager
     * @param Config $config
     * @param LoggerInterface $logger
     */
    public function __construct(
        SessionQuote $sessionQuote,
        RequestInterface $request,
        CommandManagerInterface $commandManager,
        Config $config,
        LoggerInterface $logger
    ) {
        $this->sessionQuote = $sessionQuote;
        $this->request = $request;
        $this->commandManager = $commandManager;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @param $storeId
     * @return bool
     */
    private function isApplicable($storeId = null): bool
    {
        $storeId = $storeId ?? $this->sessionQuote->getStore()->getId();
        if ($this->config->getPaymentAction($storeId) == PaymentAction::DEBIT) {
            return true;
        }

        return false;
    }


    /**
     * @param $storeId
     * @return void
     * @throws NoSuchEntityException|LocalizedException
     */
    public function resolve($storeId = null): void
    {
        if (!$this->isApplicable($storeId)) {
            return;
        }

        $this->logger->debug(
            "Check payment status before. Payment gateway response params:",
            $this->request->getParams()
        );

        $this->logger->debug("Get Command: " . PaymentStatusCommand::COMMAND_CODE);
        $command = $this->commandManager->get(PaymentStatusCommand::COMMAND_CODE);
        if (!$command instanceof CommandInterface) {
            $this->logger->critical(__("Payment Status command should be provided."), []);
            throw new CommandException(__("Payment Status command should be provided."));
        }

        $command->execute([
            PaymentStatusRequestDataBuilder::CHECKOUT_ID =>
                $this->request->getParam(PaymentStatusRequestDataBuilder::CHECKOUT_ID),
            PaymentStatusRequestDataBuilder::RESOURCE_PATH =>
                $this->request->getParam(PaymentStatusRequestDataBuilder::RESOURCE_PATH),
        ]);

        $this->logger->debug("Check payment status after.");
    }
}
