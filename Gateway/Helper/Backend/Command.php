<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Helper\Backend;

use Magento\Payment\Gateway\Command\CommandManagerInterface;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Gateway\Command\CommandException;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Gateway\Command\Backend\DebitCommand;
use TotalProcessing\Opp\Gateway\Command\PreAuthorizeCommand;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Model\System\Config\PaymentAction;
use Magento\Backend\Model\Session\Quote as SessionQuote;

/**
 * Class Command
 * @package TotalProcessing\Opp\Gateway\Helper\Backend
 */
class Command
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var CommandManagerInterface
     */
    protected $commandManager;

    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param Config $config
     * @param CommandManagerInterface $commandManager
     * @param SessionQuote $sessionQuote
     * @param LoggerInterface $logger
     */
    public function __construct(
        Config $config,
        CommandManagerInterface $commandManager,
        SessionQuote $sessionQuote,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->commandManager = $commandManager;
        $this->sessionQuote = $sessionQuote;
        $this->logger = $logger;
    }

    /**
     * Returns total processing checkout ID
     *
     * @return string
     * @throws \Exception
     */
    public function getCheckoutId(): string
    {
        try {
            $this->sessionQuote->unsCheckoutId();

            $storeId = $this->sessionQuote->getStoreId();
            $paymentAction = $this->config->getPaymentAction($storeId);
            if ($paymentAction != PaymentAction::DEBIT) {
                throw new CommandException(
                    __("Only debit payment action is implemented for the backend. Current payment action is {$paymentAction}")
                );
            }

            $this->logger->debug("Get Command: " . DebitCommand::COMMAND_CODE);
            $command = $this->commandManager->get(DebitCommand::COMMAND_CODE);
            if (!$command instanceof CommandInterface) {
                $this->logger->critical(__("Debit backend command should be provided."), []);
                throw new CommandException(__("Debit backend command should be provided."));
            }

            $quote = $this->sessionQuote->getQuote();
            $command->execute([
                'amount' => $quote->getGrandTotal(),
                'currencyCode' => $quote->getCurrency()->getQuoteCurrencyCode(),
                'entityId' => $this->config->getEntityId($storeId),
            ]);
        } catch (\Throwable $t) {
            $this->logger->critical($t->getMessage(), []);
            return '';
        }

        return (string)$this->sessionQuote->getCheckoutId();
    }

    /**
     * Returns whether scheduler applicable
     *
     * @return bool
     */
    public function isSchedulerActive(): bool
    {
        $quote = $this->sessionQuote->getQuote();
        $storeId = $this->sessionQuote->getStoreId();
        $scheduleSkuList = $this->config->getScheduleSkus($storeId);
        if (!$this->config->isSchedulerActive($storeId) || !$scheduleSkuList) {
            return false;
        }

        foreach ($quote->getAllVisibleItems() as $item) {
            $amount = $item->getQty() * $item->getPriceInclTax() - $item->getDiscountAmount();
            if (in_array($item->getSku(), $scheduleSkuList) && $amount > 0) {
                return true;
            }
        }

        return false;
    }
}
