<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Helper\Backend;

/**
 * Class FlagProvider
 * @package TotalProcessing\Opp\Gateway\Helper\Backend
 */
class FlagProvider
{
    /**
     * The parameter that determines the request modify when performing a payment transaction
     */
    const REDIRECT_SESSION_ID = 'opp_redirect_session_id';

    /**
     * The parameter that determines the quote ID when performing a payment transaction
     */
    const REDIRECT_QUOTE_ID = 'opp_redirect_quote_id';
}
