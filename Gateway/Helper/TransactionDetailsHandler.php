<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Helper;

use Magento\Framework\App\CacheInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json as Serializer;
use Magento\Payment\Gateway\Helper\ContextHelper;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\Order\Payment;
use Magento\Sales\Model\Order\Payment\Transaction;
use Magento\Vault\Api\Data\PaymentTokenFactoryInterface;
use Magento\Vault\Api\Data\PaymentTokenInterface;
use Magento\Vault\Model\CustomerTokenManagement;
use Magento\Vault\Model\Ui\VaultConfigProvider;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProvider;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProviderFactory;
use TotalProcessing\Opp\Gateway\Request\PaymentDataBuilder;
use TotalProcessing\Opp\Gateway\Response\CommonHandler;
use TotalProcessing\Opp\Gateway\SubjectReader;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterface;
use Magento\Sales\Api\Data\OrderPaymentExtensionInterfaceFactory;
use TotalProcessing\Opp\Gateway\Response;

/**
 * Class TransactionDetailsHandler
 * @package TotalProcessing\Opp\Gateway\Helper
 */
class TransactionDetailsHandler
{
    /**
     * The identifier of the payment request that can be used to reference the payment later.
     * You get this as the field id of a payment's response and then can use it as referencedPaymentId
     * in the backoffice tutorial or as the {id} part of the URL for sending referencing requests.
     */
    const PAYMENT_ID = 'id';

    /**
     * The payment type of the request.
     */
    const PAYMENT_TYPE = 'paymentType';

    /**
     * The payment brand of the request.
     */
    const PAYMENT_BRAND = 'paymentBrand';

    /**
     * The amount of the request.
     */
    const AMOUNT = 'amount';

    /**
     * The currency of the request.
     */
    const CURRENCY = 'currency';

    /**
     * Custom parameters namespace of the request.
     */
    const CUSTOM_PARAMETERS_NAMESPACE = 'customParameters';

    /**
     * Customer namespace of the request.
     */
    const CUSTOMER_NAMESPACE = 'customer';

    /**
     * Billing namespace of the request.
     */
    const BILLING_NAMESPACE = 'billing';

    /**
     * Result details namespace of the request.
     */
    const RESULT_DETAILS_NAMESPACE = 'resultDetails';

    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var MerchantTransactionIdProviderFactory
     */
    private $merchantTransactionIdProviderFactory;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @var CustomerTokenManagement
     */
    private $customerTokenManagement;

    /**
     * @var OrderPaymentExtensionInterfaceFactory
     */
    private $paymentExtensionFactory;

    /**
     * @var PaymentTokenFactoryInterface
     */
    private $paymentTokenFactory;

    /**
     * @var array
     */
    private $response = [];

    /**
     * @param CacheInterface $cache
     * @param SubjectReader $subjectReader
     * @param MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory
     * @param Serializer $serializer
     * @param CustomerTokenManagement $customerTokenManagement
     * @param OrderPaymentExtensionInterfaceFactory $paymentExtensionFactory
     * @param PaymentTokenFactoryInterface $paymentTokenFactory
     * @param array $response
     */
    public function __construct(
        CacheInterface $cache,
        SubjectReader $subjectReader,
        MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory,
        Serializer $serializer,
        CustomerTokenManagement $customerTokenManagement,
        OrderPaymentExtensionInterfaceFactory $paymentExtensionFactory,
        PaymentTokenFactoryInterface $paymentTokenFactory,
        array $response = []
    ) {
        $this->cache = $cache;
        $this->subjectReader = $subjectReader;
        $this->merchantTransactionIdProviderFactory = $merchantTransactionIdProviderFactory;
        $this->serializer = $serializer;
        $this->customerTokenManagement = $customerTokenManagement;
        $this->paymentExtensionFactory = $paymentExtensionFactory;
        $this->paymentTokenFactory = $paymentTokenFactory;
        $this->response = $response;
    }

    /**
     * Is status check request successful
     *
     * @param array $response
     * @return bool
     */
    private function isRequestSuccess(array $response): bool
    {
        $status = $response[CommonHandler::RESULT_NAMESPACE][CommonHandler::RESULT_CODE] ?? null;
        if (!$status) {
            return false;
        }

        return in_array($status, SuccessCode::getSuccessfulTransactionCodes());
    }

    /**
     * @param string $cacheKey
     * @return array
     */
    private function getResponse(string $cacheKey): array
    {
        if ($this->response) {
            return $this->response;
        }

        $response = null;
        if ($paymentStatusResponse = $this->cache->load($cacheKey)) {
            try {
                $response = $this->serializer->unserialize($paymentStatusResponse);
            } catch (\InvalidArgumentException $e) {
                // omit exception
            }
        }

        if (!$response) {
            throw new \InvalidArgumentException('The payment status response must be provided.');
        }

        if (!$this->isRequestSuccess($response)) {
            throw new \LogicException('The payment was not processed successfully.');
        }

        return $response;
    }

    /**
     * @param Payment $payment
     * @param Order $order
     * @return void
     * @throws \Exception
     */
    public function execute(Payment $payment, Order $order): void
    {
        $cacheKey = sprintf(
            '%s_%d',
            Response\PaymentStatusTransactionDetailsHandler::PAYMENT_STATUS_RESPONSE_CACHE_KEY,
            (int)$order->getQuoteId()
        );
        $response = $this->getResponse($cacheKey);

        ContextHelper::assertOrderPayment($payment);

        $this->handleResponseData($payment, $response);

        $this->cache->remove($cacheKey);
    }

    /**
     * @param InfoInterface $payment
     * @param $namespaceData
     * @param $parentNamespace
     * @return void
     */
    private function processAdditionalInformation(
        InfoInterface $payment,
        $namespaceData,
        $parentNamespace = null
    ): void {
        foreach ($namespaceData as $namespace => $value) {
            if (is_array($value)) {
                if (empty($value)) {
                    continue;
                }

                $this->processAdditionalInformation(
                    $payment,
                    $value,
                    $parentNamespace
                        ? ($parentNamespace . '.' . $namespace)
                        : $namespace
                );
                continue;
            }

            $key = $namespace;
            if ($parentNamespace) {
                $key = $parentNamespace . '.' . $namespace;
            }
            $payment->setAdditionalInformation($key, (string)$value);
        }
    }

    /**
     * Convert payment token details to JSON
     *
     * @param array $details
     * @return string
     */
    private function convertDetailsToJSON(array $details): string
    {
        $json = $this->serializer->serialize($details);
        return $json ?: '{}';
    }

    /**
     * Format card expiration date
     *
     * @param array $cardDetails
     * @return string
     * @throws \Exception
     */
    private function getExpirationDate(array $cardDetails): string
    {
        $expDate = new \DateTime(
            ($cardDetails[Response\CardDetailsHandler::CARD_EXP_YEAR] ?? '')
            . '-'
            . ($cardDetails[Response\CardDetailsHandler::CARD_EXP_MONTH] ?? '')
            . '-'
            . '01'
            . ' '
            . '00:00:00',
            new \DateTimeZone('UTC')
        );

        $expDate->add(new \DateInterval('P1M'));

        return $expDate->format('Y-m-d 00:00:00');
    }

    /**
     * Get payment extension attributes
     *
     * @param InfoInterface $payment
     * @return OrderPaymentExtensionInterface
     */
    private function getExtensionAttributes(InfoInterface $payment): OrderPaymentExtensionInterface
    {
        $extensionAttributes = $payment->getExtensionAttributes();
        if ($extensionAttributes === null) {
            $extensionAttributes = $this->paymentExtensionFactory->create();
            $payment->setExtensionAttributes($extensionAttributes);
        }

        return $extensionAttributes;
    }

    /**
     * Get vault payment token entity
     *
     * @param array $paymentData
     * @return PaymentTokenInterface|null
     * @throws \Exception
     */
    private function getVaultPaymentToken(array $paymentData): ?PaymentTokenInterface
    {
        $registrationId = $this->subjectReader->readResponse(
            $paymentData, Response\VaultDetailsHandler::REGISTRATION_ID
        ) ?? '';

        if (empty($registrationId) || $this->isTokenExists($registrationId)) {
            return null;
        }

        $cardDetails = $this->subjectReader->readResponse(
            $paymentData, Response\CardDetailsHandler::CARD_NAMESPACE
        ) ?? [];
        // If we have no 3d secure verification ID it is not 3d secure
        $data3DSecure = $this->subjectReader->readResponse(
            $paymentData,
            Response\ThreeDSecureHandler::THREE_D_SECURE_NAMESPACE
        );
        $is3DSecure = isset($data3DSecure[Response\ThreeDSecureHandler::THREE_D_SECURE_VERIFICATION_ID]);

        $paymentToken = $this->paymentTokenFactory->create(PaymentTokenFactoryInterface::TOKEN_TYPE_CREDIT_CARD);

        $paymentToken
            ->setGatewayToken($registrationId)
            ->setExpiresAt($this->getExpirationDate($cardDetails));

        $jsonDetails = $this->convertDetailsToJSON([
            'type' => $this->subjectReader->readResponse(
                    $paymentData, Response\PaymentDetailsHandler::BASIC_PAYMENT_BRAND
                ) ?? '',
            'maskedCC' => $cardDetails[Response\CardDetailsHandler::CARD_LAST4_DIGITS],
            'expirationDate' => ($cardDetails[Response\CardDetailsHandler::CARD_EXP_MONTH] ?? '')
                . "/" . ($cardDetails[Response\CardDetailsHandler::CARD_EXP_YEAR] ?? ''),
            Response\ThreeDSecureHandler::IS_THREE_D_SECURE => $is3DSecure
        ]);

        $this->subjectReader->debug("Json Details", ['Data' => $jsonDetails]);

        $paymentToken->setTokenDetails($jsonDetails);

        return $paymentToken;
    }

    /**
     * @param InfoInterface $payment
     * @return $this
     * @throws LocalizedException
     */
    private function handleMerchantTransactionId(InfoInterface $payment): self
    {
        /** @var MerchantTransactionIdProvider $merchantTransactionIdProvider */
        $merchantTransactionIdProvider = $this->merchantTransactionIdProviderFactory->create();
        $payment->setAdditionalInformation(
            PaymentDataBuilder::MERCHANT_TRANSACTION_ID,
            $merchantTransactionIdProvider->execute()
        );

        return $this;
    }

    /**
     * @param InfoInterface $payment
     * @param array $response
     * @return $this
     */
    private function handlePaymentActionTypes(InfoInterface $payment, array $response): self
    {
        $fields = [
            Response\TransactionCheckHandler::IS_PRE_AUTHORIZED,
            Response\TransactionCheckHandler::IS_CAPTURED
        ];
        foreach ($fields as $field) {
            $payment->setAdditionalInformation($field, $response);
        }

        return $this;
    }

    /**
     * Handle transaction common details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleCommonDetails(InfoInterface $payment, array $paymentData): self
    {
        $responseFields = [
            Response\CommonHandler::BUILD_NUMBER,
            Response\CommonHandler::TIMESTAMP,
            Response\CommonHandler::NDC
        ];
        foreach ($responseFields as $field) {
            $payment->setAdditionalInformation($field, $this->subjectReader->readResponse($paymentData, $field));
        }
        $payment->setAdditionalInformation(Response\CommonHandler::RESPONSE, $this->serializer->serialize($paymentData));

        return $this;
    }

    /**
     * Handle transaction customer details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handlePaymentDetails(InfoInterface $payment, array $paymentData): self
    {
        $fields = [
            Response\CommonHandler::ID,
            Response\PaymentDetailsHandler::BASIC_PAYMENT_BRAND,
            Response\PaymentDetailsHandler::BASIC_PAYMENT_TYPE,
            Response\PaymentDetailsHandler::AMOUNT,
            Response\PaymentDetailsHandler::CURRENCY,
            Response\CommonHandler::DESCRIPTOR,
            PaymentDataBuilder::MERCHANT_TRANSACTION_ID
        ];
        foreach ($fields as $field) {
            $payment->setAdditionalInformation($field, $this->subjectReader->readResponse($paymentData, $field));
        }

        return $this;
    }

    /**
     * Handle transaction state details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleTransactionState(InfoInterface $payment, array $paymentData): self
    {
        $result = $this->subjectReader->readResponse(
            $paymentData,
            Response\CommonHandler::RESULT_NAMESPACE
        ) ?? [];
        $this->processAdditionalInformation($payment, [Response\CommonHandler::RESULT_NAMESPACE => $result]);

        return $this;
    }

    /**
     * Handle transaction result details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleResultDetails(InfoInterface $payment, array $paymentData): self
    {
        $resultDetails = $this->subjectReader->readResponse($paymentData, self::RESULT_DETAILS_NAMESPACE)
            ?? [];
        $this->processAdditionalInformation($payment, [self::RESULT_DETAILS_NAMESPACE => $resultDetails]);

        return $this;
    }

    /**
     * Handle transaction card details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleCardDetails(InfoInterface $payment, array $paymentData): self
    {
        $card = $this->subjectReader->readResponse(
            $paymentData,
            Response\CardDetailsHandler::CARD_NAMESPACE
        ) ?? [];
        if ($card) {
            if (isset($card[Response\CardDetailsHandler::CARD_LAST4_DIGITS])) {
                $payment->setCcLast4($card[Response\CardDetailsHandler::CARD_LAST4_DIGITS]);
            }
            if (isset($card[Response\CardDetailsHandler::CARD_EXP_MONTH])) {
                $payment->setCcExpMonth($card[Response\CardDetailsHandler::CARD_EXP_MONTH]);
            }
            if (isset($card[Response\CardDetailsHandler::CARD_EXP_YEAR])) {
                $payment->setCcExpYear($card[Response\CardDetailsHandler::CARD_EXP_YEAR]);
            }
            if (isset($card[Response\CardDetailsHandler::CARD_HOLDER])) {
                $payment->setCcOwner($card[Response\CardDetailsHandler::CARD_HOLDER]);
            }

            $payment->setCcType(
                $this->subjectReader->readResponse(
                    $paymentData,
                    Response\PaymentDetailsHandler::BASIC_PAYMENT_BRAND
                )
            );

            $this->processAdditionalInformation($payment, [Response\CardDetailsHandler::CARD_NAMESPACE => $card]);
        }

        return $this;
    }

    /**
     * Handle transaction customer details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleCustomerDetails(InfoInterface $payment, array $paymentData): self
    {
        $customerDetails = $this->subjectReader->readResponse($paymentData, self::CUSTOMER_NAMESPACE) ?? [];
        $this->processAdditionalInformation($payment, [self::CUSTOMER_NAMESPACE => $customerDetails]);

        return $this;
    }

    /**
     * Handle transaction billing details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleBillingDetails(InfoInterface $payment, array $paymentData): self
    {
        $billingDetails = $this->subjectReader->readResponse($paymentData, self::BILLING_NAMESPACE) ?? [];
        $this->processAdditionalInformation($payment, [self::BILLING_NAMESPACE => $billingDetails]);

        return $this;
    }

    /**
     * Handle transaction custom details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleCustomDetails(InfoInterface $payment, array $paymentData): self
    {
        $customDetails = $this->subjectReader->readResponse(
            $paymentData,
            self::CUSTOM_PARAMETERS_NAMESPACE
        ) ?? [];
        $this->processAdditionalInformation(
            $payment,
            [self::CUSTOM_PARAMETERS_NAMESPACE => $customDetails]
        );

        return $this;
    }

    /**
     * Handle transaction risk details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleRiskDetails(InfoInterface $payment, array $paymentData): self
    {
        $riskData = $this->subjectReader->readResponse(
            $paymentData,
            Response\RiskDataHandler::RISK_NAMESPACE
        ) ?? [];
        $this->processAdditionalInformation($payment, [Response\RiskDataHandler::RISK_NAMESPACE => $riskData]);

        return $this;
    }

    /**
     * Handle transaction details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleTransactionDetails(InfoInterface $payment, array $paymentData): self
    {
        if (!$payment->getTransactionid()) {
            $transactionId = $this->subjectReader->readResponse(
                $paymentData,
                Response\TransactionIdHandler::TRANSACTION_ID
            );
            $payment->setTransactionId($transactionId ?? '');
        }

        $payment->setIsTransactionClosed(false);
        $payment->setShouldCloseParentTransaction(false);

        return $this;
    }

    /**
     * Handle transaction 3D secure details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     */
    private function handleThreeDSecureDetails(InfoInterface $payment, array $paymentData): self
    {
        $threeDSecureDetails = $this->subjectReader->readResponse(
            $paymentData,
            Response\ThreeDSecureHandler::THREE_D_SECURE_NAMESPACE
        ) ?? [];
        $this->processAdditionalInformation(
            $payment,
            [Response\ThreeDSecureHandler::THREE_D_SECURE_NAMESPACE => $threeDSecureDetails]
        );

        return $this;
    }

    /**
     * Handle vault details
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return $this
     * @throws \Exception
     */
    private function handleVaultDetails(InfoInterface $payment, array $paymentData): self
    {
        $paymentToken = $this->getVaultPaymentToken($paymentData);
        if (null !== $paymentToken) {
            $additionalInformation = $payment->getAdditionalInformation();
            if (!array_key_exists(VaultConfigProvider::IS_ACTIVE_CODE, $additionalInformation)) {
                $additionalInformation[VaultConfigProvider::IS_ACTIVE_CODE] = true;
                $payment->setAdditionalInformation($additionalInformation);
            }
            $extensionAttributes = $this->getExtensionAttributes($payment);
            $extensionAttributes->setVaultPaymentToken($paymentToken);
        }

        return $this;
    }

    /**
     * Handle transaction additional info
     *
     * @param InfoInterface $payment
     * @return $this
     * @throws \Exception
     */
    private function handleTransactionAdditionalInfo(InfoInterface $payment): self
    {
        $additionalInformation = $payment->getAdditionalInformation();
        if (empty($payment->getTransactionAdditionalInfo()) && !empty($additionalInformation)) {
            $unsetFields = [
                Response\CommonHandler::RESPONSE,
                Response\TransactionCheckHandler::IS_PRE_AUTHORIZED,
                Response\TransactionCheckHandler::IS_CAPTURED
            ];
            foreach ($unsetFields as $field) {
                unset($additionalInformation[$field]);
            }
            // for backend transaction details grid in case if key 'raw_details_info' was specified
            // @see \Magento\Sales\Block\Adminhtml\Transactions\Detail\Grid::getTransactionAdditionalInfo()
            $payment->setTransactionAdditionalInfo(Transaction::RAW_DETAILS, $additionalInformation);
            // for backend transaction details grid without specific key specified
            foreach ($additionalInformation as $field => $value) {
                if (!is_array($value)) {
                    $payment->setTransactionAdditionalInfo($field, $value);
                }
            };
        }

        return $this;
    }

    /**
     * Returns whether registration id exists
     *
     * @param string $registrationId
     * @return bool
     */
    private function isTokenExists(string $registrationId): bool
    {
        return (bool) array_filter(
            $this->customerTokenManagement->getCustomerSessionTokens(),
            function ($token) use ($registrationId) {
                return $token->getGatewayToken() === $registrationId;
            }
        );
    }

    /**
     * Process transaction from payment status response
     *
     * @param InfoInterface $payment
     * @param array $paymentData
     * @return void
     * @throws \Exception
     */
    private function handleResponseData(InfoInterface $payment, array $paymentData): void
    {
        $this->handleMerchantTransactionId($payment)
            ->handlePaymentActionTypes($payment, $paymentData)
            ->handleCommonDetails($payment, $paymentData)
            ->handlePaymentDetails($payment, $paymentData)
            ->handleTransactionState($payment, $paymentData)
            ->handleResultDetails($payment, $paymentData)
            ->handleCardDetails($payment, $paymentData)
            ->handleCustomerDetails($payment, $paymentData)
            ->handleBillingDetails($payment, $paymentData)
            ->handleCustomDetails($payment, $paymentData)
            ->handleRiskDetails($payment, $paymentData)
            ->handleThreeDSecureDetails($payment, $paymentData)
            ->handleTransactionDetails($payment, $paymentData)
            ->handleVaultDetails($payment, $paymentData)
            ->handleTransactionAdditionalInfo($payment);
    }
}
