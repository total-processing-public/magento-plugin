<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Helper;

/**
 * Class TransactionCategoryProvider
 * @package TotalProcessing\Opp\Gateway\Helper
 */
class TransactionCategoryProvider
{
    const BACKEND_CODE = 'MOTO';
    const FRONTEND_CODE = null;

    /**
     * Returns transaction category code for backend
     *
     * @return string
     */
    public static function getBackendTransactionCategory(): string
    {
        return self::BACKEND_CODE;
    }

    /**
     * Returns transaction category code for frontend
     *
     * @return string
     */
    public static function getFrontendTransactionCategory(): string
    {
        return self::FRONTEND_CODE;
    }
}
