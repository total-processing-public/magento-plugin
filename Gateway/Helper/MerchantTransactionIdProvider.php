<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Helper;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Backend\Model\Session\Quote as BackendSessionQuote;
use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Model\Quote;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Model\System\Config\MerchantTransactionIdType;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;

/**
 * Class MerchantTransactionIdManager
 * @package TotalProcessing\Opp\Gateway\Helper
 */
class MerchantTransactionIdProvider
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var BackendSessionQuote
     */
    private $backendSessionQuote;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var State
     */
    private $appState;

    /**
     * @param CheckoutSession $checkoutSession
     * @param BackendSessionQuote $backendSessionQuote
     * @param State $appState
     * @param Config $config
     * @param CartRepositoryInterface $cartRepository
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        BackendSessionQuote $backendSessionQuote,
        State $appState,
        Config $config,
        CartRepositoryInterface $cartRepository
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->backendSessionQuote = $backendSessionQuote;
        $this->appState = $appState;
        $this->config = $config;
        $this->cartRepository = $cartRepository;
    }

    /**
     * Get merchant transaction ID based on chosen type.
     *
     * @param Quote $quote
     * @return mixed|string|null
     * @throws LocalizedException
     */
    public function execute($quote = null)
    {
        if (null === $quote) {
            try {
                if ($this->appState->getAreaCode() === Area::AREA_ADMINHTML) {
                    $quote = $this->backendSessionQuote->getQuote();
                } else {
                    $quote = $this->checkoutSession->getQuote();
                }
            } catch (LocalizedException $e) {
                throw new LocalizedException(__('Can\'t init current quote to resolve transaction ID.'));
            }
        }

        $type = $this->config->getMerchantTransactionIdType($quote->getStoreId());
        if (
            ($type == MerchantTransactionIdType::UUID)
            && ($merchantTransactionId = $quote->getOppMerchantTransactionId())
        ) {
            return $merchantTransactionId;
        }
        if ($type == MerchantTransactionIdType::INCREMENT_ID) {
            $merchantTransactionId = $quote->getReservedOrderId();
            if (!$merchantTransactionId) {
                $quote->reserveOrderId();
                $this->cartRepository->save($quote);
                $merchantTransactionId = $quote->getReservedOrderId();
            }
            return $merchantTransactionId;
        }

        return null;
    }
}
