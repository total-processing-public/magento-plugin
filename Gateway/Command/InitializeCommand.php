<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Command;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Payment\Gateway\Command\CommandManagerInterface;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Sales\Model\Order;
use Magento\Payment\Gateway\Helper\SubjectReader;
use Magento\Sales\Model\Order\Payment;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Model\System\Config\PaymentAction;
use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use TotalProcessing\Opp\Model\CancelServiceInterface;
use TotalProcessing\Opp\Gateway\Helper\TransactionDetailsHandler;
use TotalProcessing\Opp\Gateway\Helper\TransactionDetailsHandlerFactory;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Api\WebhookOrderProcessorInterface;

/**
 * Class InitializeCommand
 * @package TotalProcessing\Opp\Gateway\Command
 */
class InitializeCommand implements CommandInterface
{
    /**
     * @var CommandManagerInterface
     */
    private $commandManager;

    /**
     * @var State
     */
    private $appState;

    /**
     * @var CancelServiceInterface
     */
    private $cancelService;

    /**
     * @var TransactionDetailsHandlerFactory
     */
    private $transactionDetailsHandlerFactory;

    /**
     * @var WebhookOrderProcessorInterface
     */
    private $webhookOrderProcessor;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CommandManagerInterface $commandManager
     * @param State $appState
     * @param CancelServiceInterface $cancelService
     * @param TransactionDetailsHandlerFactory $transactionDetailsHandlerFactory
     * @param WebhookOrderProcessorInterface $webhookOrderProcessor
     * @param Config $config
     * @param LoggerInterface $logger
     */
    public function __construct(
        CommandManagerInterface $commandManager,
        State $appState,
        CancelServiceInterface $cancelService,
        TransactionDetailsHandlerFactory $transactionDetailsHandlerFactory,
        WebhookOrderProcessorInterface $webhookOrderProcessor,
        Config $config,
        LoggerInterface $logger
    ) {
        $this->commandManager = $commandManager;
        $this->appState = $appState;
        $this->cancelService = $cancelService;
        $this->transactionDetailsHandlerFactory = $transactionDetailsHandlerFactory;
        $this->webhookOrderProcessor = $webhookOrderProcessor;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @throws CommandException
     * @throws LocalizedException
     */
    public function execute(array $commandSubject)
    {
        $paymentAction = $commandSubject['paymentAction'] ?? null;
        if (!$paymentAction) {
            throw new \InvalidArgumentException('Payment action should be provided.');
        }

        $stateObject = SubjectReader::readStateObject($commandSubject);
        $paymentDO = SubjectReader::readPayment($commandSubject);

        /** @var Payment $payment */
        $payment = $paymentDO->getPayment();
        /** @var Order $order */
        $order = $payment->getOrder();

        switch ($paymentAction) {
            case PaymentAction::AUTHORIZE:
                $this->processAuthorize($order, $payment, $stateObject);
                break;
            case PaymentAction::AUTHORIZE_CAPTURE:
                $this->processCapture($order, $payment, $stateObject, 'capture');
                break;
            case PaymentAction::DEBIT:
                $this->processDebit($order, $payment, $stateObject, $commandSubject);
                break;
            default:
                break;
        }
    }

    /**
     * @param Order $order
     * @param Payment $payment
     * @param $stateObject
     * @return void
     */
    private function processAuthorize(Order $order, Payment $payment, $stateObject)
    {
        $totalDue = $order->getTotalDue();
        $baseTotalDue = $order->getBaseTotalDue();

        $payment->authorize(true, $baseTotalDue);
        // base amount will be set inside
        $payment->setAmountAuthorized($totalDue);
        $order->setCustomerNote(__('Authorize payment action by OPP.'));
        $this->updateStateObject(
            $stateObject,
            Order::STATE_NEW,
            $order->getConfig()->getStateDefaultStatus(Order::STATE_NEW)
        );
    }

    /**
     * @param Order $order
     * @param Payment $payment
     * @param $stateObject
     * @param string $type
     * @return void
     * @throws LocalizedException
     */
    private function processCapture(Order $order, Payment $payment, $stateObject, string $type): void
    {
        $totalDue = $order->getTotalDue();
        $baseTotalDue = $order->getBaseTotalDue();

        $payment->setAmountAuthorized($totalDue);
        $payment->setBaseAmountAuthorized($baseTotalDue);
        $payment->capture();
        $order->setCustomerNote(__(ucwords($type) . ' payment action by OPP.'));
        $this->updateStateObject(
            $stateObject,
            Order::STATE_PROCESSING,
            $order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING)
        );
    }

    /**
     * @param array $commandSubject
     * @param Order $order
     * @return void
     * @throws CommandException
     * @throws LocalizedException
     * @throws NotFoundException
     */
    private function executeTransactionCheckCommand(array $commandSubject, Order $order): void
    {
        $this->logger->debug("Before execute transaction check", $commandSubject);
        try {
            $commandCode = TransactionCheckCommand::COMMAND_CODE;
            if ($this->appState->getAreaCode() === Area::AREA_ADMINHTML) {
                $commandCode = Backend\TransactionCheckCommand::COMMAND_CODE;
            }

            $command = $this->commandManager->get($commandCode);
            if (!$command instanceof CommandInterface) {
                $this->logger->critical(__("Transaction check command should be provided."), []);
                throw new CommandException(__("Transaction check command should be provided."));
            }
            $command->execute($commandSubject);
        } catch (CommandException $e) {
            // if the details of the transaction are not available or an error occurred during the request,
            // the customer will receive an error in the storefront, the order in the Magento will not be created,
            // but the transaction in the payment gateway already exist, so we need to cancel this payment
            $this->cancelService->cancelPayment(null, $order, $commandSubject);
            $this->logger->critical(__($e->getMessage()), []);
            throw new CommandException(__($e->getMessage()));
        }
        $this->logger->debug("After execute transaction check", $commandSubject);
    }

    /**
     * @param Payment $payment
     * @param Order $order
     * @param array $commandSubject
     * @return void
     * @throws CommandException
     * @throws LocalizedException
     * @throws NotFoundException
     */
    private function applyTransactionDetails(Payment $payment, Order $order, array $commandSubject): void
    {
        $this->logger->debug("Before apply transaction details.", $commandSubject);
        try {
            /** @var TransactionDetailsHandler $transactionDetailsHandler */
            $transactionDetailsHandler = $this->transactionDetailsHandlerFactory->create();
            $transactionDetailsHandler->execute($payment, $order);
        } catch (\Exception $e) {
            // if the details of the transaction are not available or an error occurred during the process,
            // the customer will receive an error in the storefront, the order in the Magento will not be created,
            // but the transaction in the payment gateway already exist, so we need to cancel this payment
            $this->cancelService->cancelPayment(null, $order, $commandSubject);
            $this->logger->critical(__($e->getMessage()), []);
            throw new CommandException(__($e->getMessage()));
        }
        $this->logger->debug("After apply transaction details.", $commandSubject);
    }

    /**
     * @param Order $order
     * @param Payment $payment
     * @param $stateObject
     * @param array $commandSubject
     * @return void
     * @throws CommandException
     * @throws LocalizedException
     * @throws NotFoundException
     */
    private function processDebit(Order $order, Payment $payment, $stateObject, array $commandSubject): void
    {
        if ($this->config->isWebhooksAvailable($order->getStoreId())) {
            // omit creating a transaction and invoice - the order will be processed using payment webhooks
            $order->setCustomerNote(__("A payment is pending for this order."));
            $this->updateStateObject(
                $stateObject,
                Order::STATE_PENDING_PAYMENT,
                $order->getConfig()->getStateDefaultStatus(Order::STATE_PENDING_PAYMENT)
            );
            $order->setOppWebhooksUpdateRequired(true);
            // don't send order confirmation email before it's actually processed by the webhook
            $order->setCanSendNewEmailFlag(false);
            return;
        }

        $this->applyTransactionDetails($payment, $order, $commandSubject);
        $this->processCapture($order, $payment, $stateObject, 'debit');
    }

    /**
     * Updates the state object
     *
     * @param object $stateObject
     * @param string $orderState
     * @param string $orderStatus
     * @return void
     */
    private function updateStateObject(object $stateObject, string $orderState, string $orderStatus): void
    {
        $stateObject->setState($orderState);
        $stateObject->setStatus($orderStatus);
        $stateObject->setIsNotified(true);
    }
}
