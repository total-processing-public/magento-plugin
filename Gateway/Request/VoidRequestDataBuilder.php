<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request;

use Laminas\Http\Request;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\Response\CommonHandler;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Model\ResourceModel\Quote as ResourceQuote;
use Magento\Framework\Session\SessionManagerInterface;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class VoidRequestDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class VoidRequestDataBuilder extends BaseRequestDataBuilder
{
    const STATUS_PATH = '/v1/payments/{id}';

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        SessionManagerInterface $sessionManager
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->sessionManager = $sessionManager;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("buildSubject Data", $buildSubject);

        $paymentId = null;
        $storeId = null;

        try {
            $paymentDataObject = $this->subjectReader->readPayment($buildSubject);
            $order = $paymentDataObject->getOrder();
            $payment = $paymentDataObject->getPayment();
            $paymentId = $payment->getAdditionalInformation(CommonHandler::ID);
            $storeId = $order->getStoreId();
        } catch (\InvalidArgumentException $e) {
            // omit exception
        } catch (\TypeError $e) {
            // omit exception
        } catch (\Exception $e) {
            // omit exception
        }

        if (!$paymentId && $quote = $this->sessionManager->getQuote()) {
            $paymentId = $quote->getData(ResourceQuote::COLUMN_PAYMENT_ID);
            $storeId = $storeId ?? $quote->getStoreId();
        }

        if (!$paymentId) {
            throw new \InvalidArgumentException('Payment ID should be provided.');
        }

        $url = rtrim($this->config->getApiUrl($storeId), '/')
            . str_replace('{id}', $paymentId, self::STATUS_PATH);

        $result = [
            self::REQUEST_DATA_NAMESPACE => [
                self::REQUEST_DATA_METHOD => Request::METHOD_POST,
                self::REQUEST_DATA_URL => $url,
                self::REQUEST_DATA_HEADERS => [
                    "Authorization" => "Bearer {$this->config->getAccessToken($storeId)}",
                ],
            ]
        ];

        $this->subjectReader->debug("Void Request Data", $result);

        return $result;
    }
}
