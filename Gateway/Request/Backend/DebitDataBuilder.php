<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request\Backend;

use Magento\Backend\Model\Session\Quote as SessionQuote;
use Magento\Payment\Helper\Formatter;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\Helper\Command as CommandHelper;
use TotalProcessing\Opp\Gateway\Helper\PaymentTokenProvider;
use TotalProcessing\Opp\Gateway\Request\BaseRequestDataBuilder;
use TotalProcessing\Opp\Gateway\Request\CustomParameterDataBuilder;
use TotalProcessing\Opp\Gateway\Request\PaymentDataBuilder;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Model\System\Config\PaymentType;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProvider;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProviderFactory;
use TotalProcessing\Opp\Gateway\Helper\TransactionCategoryProvider;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class DebitDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class DebitDataBuilder extends BaseRequestDataBuilder
{
    use Formatter;

    /**
     * The entity required to authorize the request
     * <br/>
     * <strong>CONDITIONAL</strong>
     */
    const ENTITY_ID = 'entityId';

    /**
     * The amount of the payment request.
     * <br/>
     * <strong>REQUIRED</strong>
     * <br/>
     * <br/>
     * The amount is the only amount value which is relevant. All other amount declarations like taxAmount or
     * shipping.cost are already included
     */
    const AMOUNT = 'amount';

    /**
     * The currency code of the payment amount request
     * <br>
     * <strong>REQUIRED</strong>
     */
    const CURRENCY = 'currency';

    /**
     * The payment type for the request
     * <br/>
     * <strong>REQUIRED</strong>
     */
    const PAYMENT_TYPE = 'paymentType';

    /**
     * The payment brand for the request
     * <br/>
     * <strong>OPTIONAL</strong>
     */
    const PAYMENT_BRAND = 'paymentBrand';

    /**
     * The identifier of the registration request
     * <br/>
     * <strong>REQUIRED</strong>
     */
    const REGISTRATIONS_ID = 'id';

    /**
     * Stored (registered) cards namespace
     * <br/>
     * <strong>OPTIONAL</strong>
     */
    const REGISTRATIONS_NAMESPACE = 'registrations';

    /**
     * Parameter which defines transactions area
     * * <br/>
     * <strong>REQUIRED</strong>
     */
    const TRANSACTION_CATEGORY = 'transactionCategory';

    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @var CommandHelper
     */
    private $commandHelper;

    /**
     * @var PaymentTokenProvider
     */
    private $paymentTokenProvider;

    /**
     * @var MerchantTransactionIdProviderFactory
     */
    private $merchantTransactionIdProviderFactory;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param SessionQuote $sessionQuote
     * @param CommandHelper $commandHelper
     * @param PaymentTokenProvider $paymentTokenProvider
     * @param MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        SessionQuote $sessionQuote,
        CommandHelper $commandHelper,
        PaymentTokenProvider $paymentTokenProvider,
        MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->sessionQuote = $sessionQuote;
        $this->commandHelper = $commandHelper;
        $this->paymentTokenProvider = $paymentTokenProvider;
        $this->merchantTransactionIdProviderFactory = $merchantTransactionIdProviderFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("DEBIT buildSubject data", $buildSubject);

        $quote = $this->sessionQuote->getQuote();
        $quoteId = $quote->getId();

        /** @var MerchantTransactionIdProvider $merchantTransactionIdProvider */
        $merchantTransactionIdProvider = $this->merchantTransactionIdProviderFactory->create();

        $transactionCategory = TransactionCategoryProvider::getBackendTransactionCategory();
        $result = [
            self::AMOUNT => $this->formatPrice($this->subjectReader->readAmount($buildSubject)),
            self::CURRENCY => $this->subjectReader->readCurrency($buildSubject),
            self::PAYMENT_TYPE => PaymentType::DEBIT,
            // For backend, channel will be set up to be Moto via the GW and will not run through 3ds
            self::TRANSACTION_CATEGORY => $transactionCategory,
            PaymentDataBuilder::MERCHANT_TRANSACTION_ID => $merchantTransactionIdProvider->execute(),
            "customParameters[" . CustomParameterDataBuilder::PLUGIN . "]" => $this->getMetadata(),
            "customParameters[" . CustomParameterDataBuilder::QUOTE_ID . "]" => $quoteId,
            "customParameters[" . CustomParameterDataBuilder::RETURN_URL . "]" => $this->config->getBackendSource(),
            "customParameters[" . CustomParameterDataBuilder::TRANSACTION_CATEGORY . "]" => $transactionCategory
        ];

        if (!$this->commandHelper->isSchedulerActive()) {
            $i = 0;
            foreach ($this->paymentTokenProvider->getFilteredTokens() as $token) {
                $result[self::REGISTRATIONS_NAMESPACE . "[$i]." . self::REGISTRATIONS_ID] = $token->getGatewayToken();
                $i++;
            }
        }

        $this->subjectReader->debug("DEBIT request data", $result);

        return $result;
    }
}
