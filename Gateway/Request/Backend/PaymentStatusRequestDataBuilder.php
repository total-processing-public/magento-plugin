<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request\Backend;

use Magento\Backend\Model\Session\Quote as SessionQuote;
use Laminas\Http\Request;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\Request\AuthenticationDataBuilder;
use TotalProcessing\Opp\Gateway\Request\BaseRequestDataBuilder;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class PaymentStatusRequestDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class PaymentStatusRequestDataBuilder extends BaseRequestDataBuilder
{
    const STATUS_PATH = '/v1/checkouts/{checkoutId}/payment';

    const CHECKOUT_ID = 'id';
    const RESOURCE_PATH = 'resourcePath';

    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param SessionQuote $sessionQuote
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        SessionQuote $sessionQuote
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->sessionQuote = $sessionQuote;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("PAYMENT STATUS buildSubject", $buildSubject);

        $checkoutId = $buildSubject[self::CHECKOUT_ID] ?? null;
        if (null === $checkoutId) {
            $checkoutId = $this->sessionQuote->getCheckoutId();
        }

        $resourcePath = $buildSubject[self::RESOURCE_PATH] ?? null;
        if (null === $resourcePath) {
            $resourcePath = str_replace('{checkoutId}', $checkoutId, self::STATUS_PATH);
        }

        $storeId = $this->sessionQuote->getQuote()->getStoreId();
        $url = sprintf('%s%s', rtrim($this->config->getApiUrl($storeId), '/'), $resourcePath);

        $result = [
            AuthenticationDataBuilder::ENTITY_ID => $this->config->getBackendEntityId($storeId),
            self::REQUEST_DATA_NAMESPACE => [
                self::REQUEST_DATA_METHOD => Request::METHOD_GET,
                self::REQUEST_DATA_URL => $url,
                self::REQUEST_DATA_HEADERS => [
                    "Authorization" => "Bearer {$this->config->getAccessToken($storeId)}",
                ],
            ]
        ];

        $this->subjectReader->debug("PAYMENT STATUS request", $result);

        return $result;
    }
}
