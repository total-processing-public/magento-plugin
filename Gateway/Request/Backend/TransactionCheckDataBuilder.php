<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request\Backend;

use Magento\Backend\Model\Session\Quote as SessionQuote;
use Laminas\Http\Request;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\Request\AuthenticationDataBuilder;
use TotalProcessing\Opp\Gateway\Request\BaseRequestDataBuilder;
use TotalProcessing\Opp\Gateway\Request\PaymentDataBuilder;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProvider;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProviderFactory;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class TransactionCheckDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class TransactionCheckDataBuilder extends BaseRequestDataBuilder
{
    const TRANSACTION_PATH = '/v1/query';

    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @var MerchantTransactionIdProviderFactory
     */
    private $merchantTransactionIdProviderFactory;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param SessionQuote $sessionQuote
     * @param MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        SessionQuote $sessionQuote,
        MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->sessionQuote = $sessionQuote;
        $this->merchantTransactionIdProviderFactory = $merchantTransactionIdProviderFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("Transaction Check buildSubject Data", $buildSubject);

        $quote = $this->sessionQuote->getQuote();
        $storeId = $quote->getStoreId();

        /** @var MerchantTransactionIdProvider $merchantTransactionIdProvider */
        $merchantTransactionIdProvider = $this->merchantTransactionIdProviderFactory->create();

        $url = sprintf(
            '%s%s',
            rtrim($this->config->getApiUrl($storeId), '/'),
            self::TRANSACTION_PATH
        );

        $result = [
            AuthenticationDataBuilder::ENTITY_ID => $this->config->getBackendEntityId($storeId),
            PaymentDataBuilder::MERCHANT_TRANSACTION_ID => $merchantTransactionIdProvider->execute(),
            self::REQUEST_DATA_NAMESPACE => [
                self::REQUEST_DATA_METHOD => Request::METHOD_GET,
                self::REQUEST_DATA_URL => $url,
                self::REQUEST_DATA_HEADERS => [
                    "Authorization" => "Bearer {$this->config->getAccessToken($storeId)}",
                ],
            ],
        ];

        $this->subjectReader->debug("Transaction Check Request Data", $result);

        return $result;
    }
}
