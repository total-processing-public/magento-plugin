<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request\Backend;

use Magento\Backend\Model\Session\Quote as SessionQuote;
use Laminas\Http\Request;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\Request\AuthenticationDataBuilder;
use TotalProcessing\Opp\Gateway\Request\BaseRequestDataBuilder;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class DebitRequestDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class DebitRequestDataBuilder extends BaseRequestDataBuilder
{
    const CHECKOUT_PATH = '/v1/checkouts';

    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param SessionQuote $sessionQuote
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        SessionQuote $sessionQuote
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->sessionQuote = $sessionQuote;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("DEBIT buildSubject Data", $buildSubject);

        $storeId = $this->sessionQuote->getQuote()->getStoreId();
        $url = sprintf(
            '%s%s',
            rtrim($this->config->getApiUrl($storeId), '/'),
            self::CHECKOUT_PATH
        );

        $result = [
            AuthenticationDataBuilder::ENTITY_ID => $this->config->getBackendEntityId($storeId),
            self::REQUEST_DATA_NAMESPACE => [
                self::REQUEST_DATA_METHOD => Request::METHOD_POST,
                self::REQUEST_DATA_URL => $url,
                self::REQUEST_DATA_HEADERS => [
                    "Authorization" => "Bearer {$this->config->getAccessToken($storeId)}",
                ],
            ]
        ];

        $this->subjectReader->debug("Debit request data", $result);

        return $result;
    }
}
