<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request;

use Magento\Payment\Helper\Formatter;
use TotalProcessing\Opp\Model\System\Config\PaymentType;

/**
 * Class CancelDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class CancelDataBuilder extends BaseRequestDataBuilder
{
    use Formatter;

    /**
     * The entity required to authorize the request
     * <br/>
     * <strong>CONDITIONAL</strong>
     */
    const ENTITY_ID = 'entityId';

    /**
     * The amount of the payment request.
     * <br/>
     * <strong>REQUIRED</strong>
     * <br/>
     * <br/>
     * The amount is the only amount value which is relevant. All other amount declarations like taxAmount or
     * shipping.cost are already included
     */
    const AMOUNT = 'amount';

    /**
     * The currency code of the payment amount request
     * <br>
     * <strong>REQUIRED</strong>
     */
    const CURRENCY = 'currency';

    /**
     * The payment type for the request
     * <br/>
     * <strong>REQUIRED</strong>
     */
    const PAYMENT_TYPE = 'paymentType';

    /**
     * The payment brand for the request
     * <br/>
     * <strong>REQUIRED</strong>
     */
    const PAYMENT_BRAND = 'paymentBrand';

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("CANCEL buildSubject data", $buildSubject);

        $result = [
            self::PAYMENT_TYPE => PaymentType::REFUND,
            self::AMOUNT => $this->subjectReader->readAmount($buildSubject),
            self::CURRENCY => $this->subjectReader->readCurrency($buildSubject),
            "customParameters[" . CustomParameterDataBuilder::PLUGIN . "]" => $this->getMetadata()
        ];

        $this->subjectReader->debug("CANCEL request data", $result);

        return $result;
    }
}
