<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request;

use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Model\System\Config\PaymentType;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProvider;
use TotalProcessing\Opp\Gateway\Helper\MerchantTransactionIdProviderFactory;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class ReversalDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class ReversalDataBuilder extends BaseRequestDataBuilder
{
    /**
     * The payment type for the request
     * <br/>
     * <strong>REQUIRED</strong>
     */
    const PAYMENT_TYPE = 'paymentType';

    /**
     * @var MerchantTransactionIdProviderFactory
     */
    private $merchantTransactionIdProviderFactory;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        MerchantTransactionIdProviderFactory $merchantTransactionIdProviderFactory
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->merchantTransactionIdProviderFactory = $merchantTransactionIdProviderFactory;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("buildSubject data", $buildSubject);

        $paymentDataObject = $this->subjectReader->readPayment($buildSubject);

        $order = $paymentDataObject->getOrder();
        $payment = $paymentDataObject->getPayment();

        if (!$merchantTransactionId = $payment->getAdditionalInformation(PaymentDataBuilder::MERCHANT_TRANSACTION_ID)) {
            /** @var MerchantTransactionIdProvider $merchantTransactionIdProvider */
            $merchantTransactionIdProvider = $this->merchantTransactionIdProviderFactory->creare();
            $merchantTransactionId = $merchantTransactionIdProvider->execute();
        }

        $quoteId = $payment->getAdditionalInformation("customParameters_" . CustomParameterDataBuilder::QUOTE_ID);
        $returnUrl = $payment->getAdditionalInformation("customParameters_" . CustomParameterDataBuilder::RETURN_URL);

        $params = [
            self::PAYMENT_TYPE => PaymentType::REVERSAL,
            PaymentDataBuilder::MERCHANT_TRANSACTION_ID => $merchantTransactionId,
            "customParameters[" . CustomParameterDataBuilder::PLUGIN . "]" => $this->getMetadata(),
            "customParameters[" . CustomParameterDataBuilder::QUOTE_ID . "]" => $quoteId,
            "customParameters[" . CustomParameterDataBuilder::RETURN_URL . "]" => $returnUrl,
        ];

        try {
            $params["customParameters[" . CustomParameterDataBuilder::ORDER_ID . "]"] = $order->getId();
            $params["customParameters[" . CustomParameterDataBuilder::ORDER_INCREMENT_ID . "]"] = $order->getOrderIncrementId();
        } catch (\TypeError $e) {
            // omit order custom parameters
        } catch (\Exception $e) {
            // omit order custom parameters
        }

        $this->subjectReader->debug("Reversal Data", $params);

        return $params;
    }
}
