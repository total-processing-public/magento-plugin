<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request;

use Magento\Checkout\Model\Session;
use Magento\Payment\Gateway\Request\BuilderInterface;
use TotalProcessing\Opp\Gateway\SubjectReader;

/**
 * Class CustomerDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class CustomerDataBuilder implements BuilderInterface
{
    /**
     * Customer forename
     */
    const CUSTOMER_GIVEN_NAME = 'customer.givenName';

    /**
     * Customer surname
     */
    const CUSTOMER_SURNAME = 'customer.surname';

    /**
     * Customer phone
     */
    const CUSTOMER_PHONE = 'customer.phone';

    /**
     * Customer email
     */
    const CUSTOMER_EMAIL = 'customer.email';

    /**
     * Billing address city param
     */
    const BILLING_CITY = 'billing.city';

    /**
     * Billing address country param
     */
    const BILLING_COUNTRY = 'billing.country';

    /**
     * Billing address street (line 1) param
     */
    const BILLING_STREET1 = 'billing.street1';

    /**
     * Billing address postcode param
     */
    const BILLING_POSTCODE = 'billing.postcode';

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * CustomerDataBuilder Constructor
     *
     * @param SubjectReader $subjectReader
     * @param Session $checkoutSession
     */
    public function __construct(SubjectReader $subjectReader, Session $checkoutSession)
    {
        $this->subjectReader = $subjectReader;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @inheritdoc
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("Customer buildSubject Data", $buildSubject);

        $quote = $this->checkoutSession->getQuote();
        $billingAddress = $quote->getBillingAddress();
        $shippingAddress = $quote->getShippingAddress();

        $params = [
            self::CUSTOMER_GIVEN_NAME => $billingAddress->getFirstname(),
            self::CUSTOMER_SURNAME => $billingAddress->getLastname(),
            self::CUSTOMER_PHONE => $billingAddress->getTelephone(),
            self::CUSTOMER_EMAIL => $quote->getCustomerEmail()
                ?? ($shippingAddress->getEmail() ?? $billingAddress->getEmail()),
            self::BILLING_CITY => urlencode($billingAddress->getCity()),
            self::BILLING_COUNTRY => $billingAddress->getCountryId(),
            self::BILLING_STREET1 => urlencode($billingAddress->getStreetLine(1)),
            self::BILLING_POSTCODE => $billingAddress->getPostCode()
        ];

        if ($customerName = trim($billingAddress->getName())) {
            $params[CardDataBuilder::CARD_HOLDER] = $customerName;
        }

        $this->subjectReader->debug("Customer Request Data", $params);
        return array_filter($params, function ($param) {
            return $param !== null;
        });
    }
}
