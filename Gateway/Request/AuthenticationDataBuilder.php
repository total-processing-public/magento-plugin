<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Gateway\Request\BuilderInterface;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Gateway\Helper\TransactionCategoryProvider;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\App\RequestInterface;

/**
 * Class AuthenticationDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request
 */
class AuthenticationDataBuilder implements BuilderInterface
{
    /**
     * The entity required to authorize the request
     * <br/>
     * <strong>CONDITIONAL</strong>
     */
    const ENTITY_ID = 'entityId';

    /**
     * @var Config
     */
    private $config;

    /**
     * @var SubjectReader
     */
    private $subjectReader;

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @var State
     */
    private $appState;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var string|null
     */
    private $salesOrderCreateSaveHandler;

    /**
     * @param Config $config
     * @param SubjectReader $subjectReader
     * @param SessionManagerInterface $sessionManager
     * @param State $appState
     * @param RequestInterface $request
     * @param string|null $salesOrderCreateSaveHandler
     */
    public function __construct(
        Config $config,
        SubjectReader $subjectReader,
        SessionManagerInterface $sessionManager,
        State $appState,
        RequestInterface $request,
        string $salesOrderCreateSaveHandler = null
    ) {
        $this->config = $config;
        $this->subjectReader = $subjectReader;
        $this->sessionManager = $sessionManager;
        $this->appState = $appState;
        $this->request = $request;
        $this->salesOrderCreateSaveHandler = $salesOrderCreateSaveHandler;
    }

    /**
     * @return string
     * @throws LocalizedException
     */
    private function initEntityId(): string
    {
        $storeId = null;
        if ($quote = $this->sessionManager->getQuote()) {
            $storeId = $quote->getStoreId();
        }

        // standard entity ID by default
        $entityId = $this->config->getEntityId($storeId);
        if (
            ($this->appState->getAreaCode() === Area::AREA_ADMINHTML)
            && ($this->request->getFullActionName() === $this->salesOrderCreateSaveHandler)
            && $this->config->getBackendEntityId($storeId)
        ) {
            // if the refund was initiated when the order was created in the admin and an any error occurred.
            // at the same time it is also necessary to check whether the backend entity ID is available.
            $entityId = $this->config->getBackendEntityId($storeId);
        }

        return (string)$entityId;
    }

    /**
     * @param array $buildSubject
     * @return string[]
     * @throws LocalizedException
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("AUTHENTICATION buildSubject", $buildSubject);

        $entityId = $this->initEntityId();
        try {
            // an additional attempt to take the correct entity ID (in area context frontend/backend)
            // from the transaction details (if any)
            $paymentDataObject = $this->subjectReader->readPayment($buildSubject);
            $order = $paymentDataObject->getOrder();
            $payment = $paymentDataObject->getPayment();

            $transactionCategory = $payment->getAdditionalInformation(
                "customParameters." . CustomParameterDataBuilder::TRANSACTION_CATEGORY
            );

            if (
                $transactionCategory
                && ($transactionCategory == TransactionCategoryProvider::getBackendTransactionCategory())
            ) {
                $entityId = $this->config->getBackendEntityId($order->getStoreId());
            }
        } catch (\InvalidArgumentException $e) {
            // omit exception
        } catch (\TypeError $e) {
            // omit exception
        } catch (\Exception $e) {
            // omit exception
        }

        if (!$entityId) {
            throw new \InvalidArgumentException('Entity ID is not provided.');
        }

        $result = [
            self::ENTITY_ID => $entityId,
        ];

        $this->subjectReader->debug("AUTHENTICATION request", $result);

        return $result;
    }
}
