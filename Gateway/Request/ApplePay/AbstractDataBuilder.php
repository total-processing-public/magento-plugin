<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request\ApplePay;

use Magento\Payment\Gateway\Request\BuilderInterface;
use TotalProcessing\Opp\Gateway\Config\ApplePay\Config;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class AbstractDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request\ApplePay
 */
abstract class AbstractDataBuilder implements BuilderInterface
{
    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Metadata
     */
    protected $metadata;

    /**
     * @var SubjectReader
     */
    protected $subjectReader;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader
    ) {
        $this->config = $config;
        $this->metadata = $metadata;
        $this->subjectReader = $subjectReader;
    }

    /**
     * @return string
     */
    protected function getMetadata(): string
    {
        return sprintf(
            '%s / %s',
            $this->metadata->getAppMetadata(null, true),
            $this->metadata->getModuleMetadata(null, null, true)
        );
    }
}
