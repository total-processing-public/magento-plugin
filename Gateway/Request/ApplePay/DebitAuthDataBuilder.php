<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request\ApplePay;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Serialize\Serializer\Json as Serializer;
use TotalProcessing\Opp\Gateway\Config\ApplePay\Config;
use TotalProcessing\Opp\Gateway\Request\BaseRequestDataBuilder as DataBuilder;
use TotalProcessing\Opp\Gateway\Request\CustomParameterDataBuilder;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Observer\DataAssignObserver;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class DebitAuthDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request\ApplePay
 */
class DebitAuthDataBuilder extends AuthDataBuilder
{
    const BILLING_COUNTRY = 'billing.country';
    const PAYMENT_TOKEN = 'applePay.paymentToken';
    const SESSION_DECRYPT_PATH = '/decrypt';
    const SHOPPER_ENDPOINT = 'customParameters[SHOPPER_endpoint]';

    /**
     * @var CheckoutSession
     */
    protected $checkoutSession;

    /**
     * @var Serializer
     */
    protected $serializer;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param CheckoutSession $checkoutSession
     * @param Serializer $serializer
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        CheckoutSession $checkoutSession,
        Serializer $serializer
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->checkoutSession = $checkoutSession;
        $this->serializer = $serializer;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("buildSubject Data", $buildSubject);

        $paymentDataObject = $this->subjectReader->readPayment($buildSubject);

        $order = $paymentDataObject->getOrder();
        $payment = $paymentDataObject->getPayment();

        $storeId = $order->getStoreId();
        $quoteId = $this->checkoutSession->getQuoteId();

        $url = rtrim($this->config->getApiUrl($storeId), '/') . self::SESSION_DECRYPT_PATH;

        $result = [
            self::PAYMENT_TOKEN => $this->serializer->unserialize(
                $payment->getAdditionalInformation(DataAssignObserver::TOKEN)
            ),
            self::SHOPPER_ENDPOINT => $this->config->getShopperEndpoint($storeId),
            DataBuilder::REQUEST_DATA_NAMESPACE => [
                DataBuilder::REQUEST_ENCODE => true,
                DataBuilder::REQUEST_DATA_URL => $url,
                DataBuilder::REQUEST_DATA_HEADERS => [
                    "Authorization" => "Bearer {$this->config->getAccessToken($storeId)}",
                ]
            ],
            "customParameters[" . CustomParameterDataBuilder::PLUGIN . "]" => $this->getMetadata(),
            "customParameters[" . CustomParameterDataBuilder::QUOTE_ID . "]" => $quoteId,
        ];

        try {
            $result["customParameters[" . CustomParameterDataBuilder::ORDER_ID . "]"] = $order->getId();
            $result["customParameters[" . CustomParameterDataBuilder::ORDER_INCREMENT_ID . "]"] = $order->getOrderIncrementId();
        } catch (\TypeError $e) {
            // The order may not have been specified yet, so the type error may occur due to PayPal Braintree overrides
            // where data return type of the methods is explicitly specified.
            // @see vendor/paypal/module-braintree-core/etc/di.xml (line 1196)
            // Examples:
            // @see \PayPal\Braintree\Gateway\Data\Order\OrderAdapter::getId()
            // method may return null instead of int
            // @see \PayPal\Braintree\Gateway\Data\Order\OrderAdapter::getOrderIncrementId()
            // method may return null instead of string
        }

        $result = array_replace_recursive(parent::build($buildSubject), $result);

        $this->subjectReader->debug("Debit Request Data", $result);

        return $result;
    }
}
