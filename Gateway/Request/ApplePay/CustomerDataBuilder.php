<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Gateway\Request\ApplePay;

use Magento\Checkout\Model\Session as CheckoutSession;
use TotalProcessing\Opp\Gateway\Config\ApplePay\Config;
use TotalProcessing\Opp\Gateway\Request\CardDataBuilder;
use TotalProcessing\Opp\Gateway\SubjectReader;
use TotalProcessing\Opp\Helper\Metadata;

/**
 * Class DebitAuthDataBuilder
 * @package TotalProcessing\Opp\Gateway\Request\ApplePay
 */
class CustomerDataBuilder extends AbstractDataBuilder
{
    /**
     * Customer forename
     */
    const CUSTOMER_GIVEN_NAME = 'customer.givenName';

    /**
     * Customer surname
     */
    const CUSTOMER_SURNAME = 'customer.surname';

    /**
     * Customer phone
     */
    const CUSTOMER_PHONE = 'customer.phone';

    /**
     * Customer email
     */
    const CUSTOMER_EMAIL = 'customer.email';

    /**
     * Billing address city param
     */
    const BILLING_CITY = 'billing.city';

    /**
     * Billing address country param
     */
    const BILLING_COUNTRY = 'billing.country';

    /**
     * Billing address street (line 1) param
     */
    const BILLING_STREET1 = 'billing.street1';

    /**
     * Billing address postcode param
     */
    const BILLING_POSTCODE = 'billing.postcode';

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @param Config $config
     * @param Metadata $metadata
     * @param SubjectReader $subjectReader
     * @param CheckoutSession $checkoutSession
     */
    public function __construct(
        Config $config,
        Metadata $metadata,
        SubjectReader $subjectReader,
        CheckoutSession $checkoutSession
    ) {
        parent::__construct($config, $metadata, $subjectReader);
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * {@inheritdoc}
     */
    public function build(array $buildSubject): array
    {
        $this->subjectReader->debug("Customer buildSubject Data", $buildSubject);

        $quote = $this->checkoutSession->getQuote();
        $billingAddress = $quote->getBillingAddress();
        $shippingAddress = $quote->getShippingAddress();

        $params = [
            self::CUSTOMER_GIVEN_NAME => $billingAddress->getFirstname(),
            self::CUSTOMER_SURNAME => $billingAddress->getLastname(),
            self::CUSTOMER_PHONE => $billingAddress->getTelephone(),
            self::CUSTOMER_EMAIL => $quote->getCustomerEmail()
                ?? ($shippingAddress->getEmail() ?? $billingAddress->getEmail()),
            self::BILLING_CITY => $billingAddress->getCity(),
            self::BILLING_COUNTRY => $billingAddress->getCountryId(),
            self::BILLING_STREET1 => $billingAddress->getStreetLine(1),
            self::BILLING_POSTCODE => $billingAddress->getPostCode()
        ];

        if ($customerName = trim($billingAddress->getName())) {
            $params[CardDataBuilder::CARD_HOLDER] = $customerName;
        }

        $this->subjectReader->debug("Customer Request Data", $params);
        return array_filter($params, function ($param) {
            return $param !== null;
        });
    }
}
