<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use TotalProcessing\Opp\Gateway\Config\Config;

/**
 * Class DataAssignObserver
 * @package TotalProcessing\Opp\Observer
 */
class QuoteSubmitSuccessObserver implements ObserverInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function __construct(
        Config $config
    ) {
        $this->config = $config;
    }

    /**
     * Prolong quote in some specific cases (customer reload the page, returns to the catalog, etc.)
     *
     * @param Quote $quote
     * @param Order $order
     * @return bool
     */
    private function isProlongQuote(Quote $quote, Order $order): bool
    {
        return $this->config->isWebhooksAvailable($order->getStoreId())
            && !$quote->getIsActive()
            && $order->getOppWebhooksUpdateRequired();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(Observer $observer)
    {
        /** @var  Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        /** @var  Order $order */
        $order = $observer->getEvent()->getOrder();

        if ($this->isProlongQuote($quote, $order)) {
            $quote->setIsActive(true);
        }
    }
}
