<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model;

use TotalProcessing\Opp\Model\ValidationRules\PaymentValidationRuleInterface;

/**
 * Class PaymentValidator
 * @package TotalProcessing\Opp\Model
 */
class PaymentValidator
{
    /**
     * @var PaymentValidationRuleInterface
     */
    private $paymentValidationRule;

    /**
     * @param PaymentValidationRuleInterface $paymentValidationRule
     */
    public function __construct(
        PaymentValidationRuleInterface $paymentValidationRule
    ) {
        $this->paymentValidationRule = $paymentValidationRule;
    }

    /**
     * @param bool $beforeRender
     * @return array
     */
    public function validate(bool $beforeRender = false): array
    {
        $result = [
            'isValid' => true
        ];
        foreach ($this->paymentValidationRule->validate($beforeRender) as $validationResult) {
            if ($validationResult->isValid()) {
                continue;
            }

            $messages = $validationResult->getErrors();
            $defaultMessage = array_shift($messages);
            if ($defaultMessage && !empty($messages)) {
                $defaultMessage .= ' %1';
            }
            if ($defaultMessage) {
                $result['isValid'] = false;
                $messages = (string)__($defaultMessage, implode(' ', $messages));
                if (!isset($result['messages'])) {
                    $result['messages'] = $messages;
                } else {
                    $result['messages'] = $result['messages'] . "<br/><br/>" . $messages;
                }
            }
        }

        return $result;
    }
}
