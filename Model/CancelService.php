<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model;

use Magento\Framework\App\Area;
use Magento\Framework\App\State;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Payment\Gateway\Command\CommandManagerInterface;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Model\Ui\ApplePay\ConfigProvider as ApplePayConfigProvider;
use TotalProcessing\Opp\Gateway\Config\Config;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Session\SessionManagerInterface;
use TotalProcessing\Opp\Model\ResourceModel\Quote as ResourceQuote;
use Magento\Checkout\Helper\Data as CheckoutHelperData;

/**
 * Class CancelService
 * @package TotalProcessing\Opp\Model
 */
class CancelService implements CancelServiceInterface
{
    const GATEWAY_CALLBACK_CANCEL = 'cancel';

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var SessionManagerInterface
     */
    private $sessionManager;

    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var CommandManagerInterface
     */
    private $defaultCommandManager;

    /**
     * @var CommandManagerInterface
     */
    private $applePayCommandManager;

    /**
     * @var CheckoutHelperData
     */
    private $checkoutHelperData;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var CommandManagerInterface
     */
    private $commandManager = null;

    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param CommandManagerInterface $defaultCommandManager
     * @param CommandManagerInterface $applePayCommandManager
     * @param CartRepositoryInterface $quoteRepository
     * @param CheckoutHelperData $checkoutHelperData
     * @param LoggerInterface $logger
     * @param SessionManagerInterface $sessionManager
     */
    public function __construct(
        OrderRepositoryInterface $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        CommandManagerInterface $defaultCommandManager,
        CommandManagerInterface $applePayCommandManager,
        CartRepositoryInterface $quoteRepository,
        CheckoutHelperData $checkoutHelperData,
        LoggerInterface $logger,
        SessionManagerInterface $sessionManager
    ) {
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->defaultCommandManager = $defaultCommandManager;
        $this->applePayCommandManager = $applePayCommandManager;
        $this->quoteRepository = $quoteRepository;
        $this->checkoutHelperData = $checkoutHelperData;
        $this->logger = $logger;
        $this->sessionManager = $sessionManager;
    }

    /**
     * @return CommandManagerInterface
     */
    private function getCommandManager(): CommandManagerInterface
    {
        return $this->commandManager;
    }

    /**
     * @param string $paymentMethod
     * @return void
     */
    private function setCommandManager(string $paymentMethod): void
    {
        $this->commandManager = $this->defaultCommandManager;
        if ($paymentMethod == ApplePayConfigProvider::CODE) {
            $this->commandManager = $this->applePayCommandManager;
        }
    }

    /**
     * @return void
     */
    private function resetCommandManager()
    {
        $this->commandManager = null;
    }

    /**
     * @param array $commandSubject
     * @param Quote|null $quote
     * @param Order|null $order
     * @return void
     */
    private function prepareCommandSubject(array &$commandSubject, Quote $quote = null, Order $order = null): void
    {
        if (!isset($commandSubject['amount'])) {
            if ($quote) {
                $commandSubject['amount'] = $quote->getGrandTotal();
            } elseif ($order) {
                $commandSubject['amount'] = $order->getGrandTotal();
            }
        }
        if (!isset($commandSubject['currencyCode'])) {
            if ($quote) {
                $commandSubject['currencyCode'] = $quote->getQuoteCurrencyCode();
            } elseif ($order) {
                $commandSubject['currencyCode'] = $order->getOrderCurrency()->getCurrencyCode();
            }
        }
    }

    /**
     * @param array $commandSubject
     * @param Quote|null $quote
     * @param Order|null $order
     * @return void
     * @throws CommandException
     * @throws NotFoundException
     */
    private function executeCancelPaymentCommand(
        array $commandSubject,
        Quote $quote = null,
        Order $order = null
    ): void {
        $this->prepareCommandSubject($commandSubject, $quote, $order);

        $this->logger->debug("Before execute cancel", $commandSubject);
        try {
            $command = $this->getCommandManager()->get(self::GATEWAY_CALLBACK_CANCEL);
            if (!$command instanceof CommandInterface) {
                $this->logger->critical(__("Cancel command should be provided."), []);
                throw new CommandException(__("Cancel command should be provided."));
            }
            $command->execute($commandSubject);
        } catch (CommandException $e) {
            $this->logger->critical(__($e->getMessage()), []);
            throw new CommandException(__($e->getMessage()));
        }
        $this->logger->debug("After execute cancel", $commandSubject);
    }

    /**
     * @inheritDoc
     */
    public function cancelPayment(
        Quote $quote = null,
        Order $order = null,
        array $commandSubject = [],
        string $message = null
    ): void {
        if (null === $quote) {
            $quote = $this->sessionManager->getQuote();
        }

        if (!$quote || !$quote instanceof Quote) {
            $this->logger->critical(__('Can\'t find quote for cancel payment.'), []);
            return;
        }
        // check if the payment has already been made
        if (!$quote->getData(ResourceQuote::COLUMN_PAYMENT_ID)) {
            return;
        }
        // check if payment was already canceled from another places (events, plugins, methods)
        if ($quote->getOppCancelPaymentProcessed()) {
            return;
        }

        $paymentMethod = (string)$quote->getPayment()->getMethod();
        try {
            $this->setCommandManager($paymentMethod);
            $this->executeCancelPaymentCommand($commandSubject, $quote, $order);
            $this->resetCommandManager();
            // mark as processed to prevent canceled payment from another places (events, plugins, methods)
            $quote->setOppCancelPaymentProcessed(true);
        } catch (CommandException $e) {
            throw new CommandException(__($e->getMessage()));
        }

        if ($message) {
            $this->checkoutHelperData->sendPaymentFailedEmail($quote, $message);
        }
    }

    /**
     * @param int $cartId
     * @param string|null $incrementId
     * @return OrderInterface|null
     */
    private function getOrder(int $cartId, string $incrementId = null): ?OrderInterface
    {
        // by default set order filter params for search criteria
        $filterField = OrderInterface::INCREMENT_ID;
        $filterValue = $incrementId;

        if (!$incrementId) {
            $hasIncrementId = false;
            // try to retrieve order from session
            $order = $this->sessionManager->getLastRealOrder() ?? $this->sessionManager->getOrder();
            if ($order && ($incrementId = $order->getIncrementId())) {
                $filterValue = $incrementId;
                $hasIncrementId = true;
            }

            if (!$hasIncrementId) {
                // set quote filter params for search criteria
                $filterField = OrderInterface::QUOTE_ID;
                $filterValue = $cartId;
            }
        }

        $searchCriteria = $this->searchCriteriaBuilder->addFilter($filterField, $filterValue)->create();
        $items = $this->orderRepository->getList($searchCriteria)->getItems();

        return array_pop($items);
    }

    /**
     * @param int|null $cartId
     * @param string|null $incrementId
     * @param string|null $message
     * @return void
     * @throws CommandException
     * @throws LocalizedException
     * @throws NotFoundException
     */
    public function cancelOrder(int $cartId = null, string $incrementId = null, string $message = null): void
    {
        if (null === $cartId) {
            $cartId = (int)$this->sessionManager->getQuote()->getId();
        }

        $order = $this->getOrder($cartId, $incrementId);
        if (null === $order) {
            // there is no order, but the payment can already be created
            // try to detect and cancel payment
            try {
                $quote = $this->quoteRepository->get($cartId);
            } catch (\Exception $e) {
                $quote = null;
            }
            $this->cancelPayment($quote, null, [], $message);
            return;
        }

        $this->logger->info(__('Cancel order '. $order->getIncrementId()), []);
        try {
            $order->cancel();
            if ($order->getState() != Order::STATE_CANCELED) {
                $order->setState(Order::STATE_CANCELED)
                    ->setStatus(Order::STATE_CANCELED);
            }
            $this->orderRepository->save($order);
        } catch (\Exception $e) {
            $this->logger->critical(__('Cancel order error. '. $e->getMessage()), []);
        }
    }
}
