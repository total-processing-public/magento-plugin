<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Quote\Model\Quote;
use Magento\Sales\Model\Order;

/**
 * Interface CancelServiceInterface
 * @package TotalProcessing\Opp\Model
 */
interface CancelServiceInterface
{
    /**
     * Cancel payment (cover not success payment place case)
     *
     * @param Quote|null $quote
     * @param Order|null $order
     * @param array $commandSubject
     * @param string|null $message
     * @return void
     * @throws CommandException
     * @throws NotFoundException
     * @throws LocalizedException
     */
    public function cancelPayment(
        Quote $quote = null,
        Order $order = null,
        array $commandSubject = [],
        string $message = null
    ): void;

    /**
     * Cancel order (cover not success order place case)
     *
     * @param int|null $cartId
     * @param string|null $incrementId
     * @param string|null $message
     * @return void
     */
    public function cancelOrder(int $cartId = null, string $incrementId = null, string $message = null): void;
}
