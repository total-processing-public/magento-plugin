<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model\Webhook\Order;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Sales\Api\Data\OrderInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order\Email\Container\OrderIdentity;
use Magento\Sales\Model\Order\Email\Container\InvoiceIdentity;
use Magento\Sales\Model\Order\Email\Sender\InvoiceSender;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Sales\Model\Order\Payment;
use TotalProcessing\Opp\Api\WebhookOrderProcessorInterface;
use Magento\Framework\Serialize\SerializerInterface;
use TotalProcessing\Opp\Exception\WebhookException;
use Magento\Store\Model\StoreManagerInterface;
use TotalProcessing\Opp\Gateway\Config\Config;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\App\RequestInterface;
use TotalProcessing\Opp\Gateway\Helper\TransactionDetailsHandler;
use TotalProcessing\Opp\Gateway\Helper\TransactionDetailsHandlerFactory;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\DB\Transaction;
use Magento\Framework\DB\TransactionFactory;
use Psr\Log\LoggerInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use TotalProcessing\Opp\Gateway\Request\CustomParameterDataBuilder;
use TotalProcessing\Opp\Model\Webhook\Order\StoreIdentifierChecker;

/**
 * Class OrderManagement
 * @package TotalProcessing\Opp\Model\Webhook
 */
class Processor implements WebhookOrderProcessorInterface
{
    /**
     * Request headers
     */
    const HEADER_X_INITIALIZATION_VECTOR = 'X-Initialization-Vector';
    const HEADER_X_AUTHENTICATION_TAG = 'X-Authentication-Tag';

    /**
     * The cipher method
     */
    const OPENSSL_DECRYPT_CIPHER_ALGO = 'aes-256-gcm';

    /**
     * Notification type keys
     */
    const NOTIFICATION_TYPE_PAYMENT = 'PAYMENT';
    const NOTIFICATION_TYPE_REGISTRATION = 'REGISTRATION';
    const NOTIFICATION_TYPE_TEST = 'test';

    /**
     * Registration action keys
     */
    const REGISTRATION_ACTION_CREATED = 'CREATED';
    const REGISTRATION_ACTION_UPDATED = 'UPDATED';
    const REGISTRATION_ACTION_DELETED = 'DELETED';

    /**
     * Payload keys
     */
    const PAYLOAD = 'payload';
    const PAYLOAD_KEY_CUSTOM_PARAMETERS = 'customParameters';
    const PAYLOAD_KEY_PAYMENT_TYPE = 'paymentType';

    /**
     * Payload payment types
     */
    const PAYLOAD_PAYMENT_TYPE_DEBIT = 'DB';
    const PAYLOAD_PAYMENT_TYPE_REFUND = 'RF';

    /**
     * @var TransactionDetailsHandlerFactory
     */
    private $transactionDetailsHandlerFactory;

    /**
     * @var OrderFactory
     */
    private $orderFactory;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var TransactionFactory
     */
    private $transactionFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var StoreIdentifierChecker
     */
    private $storeIdentifierChecker;

    /**
     * @var OrderIdentity
     */
    private $orderIdentity;

    /**
     * @var OrderSender
     */
    private $orderSender;

    /**
     * @var InvoiceIdentity
     */
    private $invoiceIdentity;

    /**
     * @var InvoiceSender
     */
    private $invoiceSender;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string|null
     */
    private $notificationType = null;

    /**
     * @var bool
     */
    private $isTestWebhook = false;

    /**
     * @var null
     */
    private $quoteId = null;

    /**
     * @var bool
     */
    private $isStoreRelevant = true;

    /**
     * @param TransactionDetailsHandlerFactory $transactionDetailsHandlerFactory
     * @param OrderFactory $orderFactory
     * @param OrderRepositoryInterface $orderRepository
     * @param CartRepositoryInterface $cartRepository
     * @param TransactionFactory $transactionFactory
     * @param RequestInterface $request
     * @param SerializerInterface $serializer
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     * @param StoreIdentifierChecker $storeIdentifierChecker
     * @param OrderIdentity $orderIdentity
     * @param OrderSender $orderSender
     * @param InvoiceIdentity $invoiceIdentity
     * @param InvoiceSender $invoiceSender
     * @param LoggerInterface $logger
     */
    public function __construct(
        TransactionDetailsHandlerFactory $transactionDetailsHandlerFactory,
        OrderFactory $orderFactory,
        OrderRepositoryInterface $orderRepository,
        CartRepositoryInterface $cartRepository,
        TransactionFactory $transactionFactory,
        RequestInterface $request,
        SerializerInterface $serializer,
        StoreManagerInterface $storeManager,
        Config $config,
        StoreIdentifierChecker $storeIdentifierChecker,
        OrderIdentity $orderIdentity,
        OrderSender $orderSender,
        InvoiceIdentity $invoiceIdentity,
        InvoiceSender $invoiceSender,
        LoggerInterface $logger
    ) {
        $this->transactionDetailsHandlerFactory = $transactionDetailsHandlerFactory;
        $this->orderFactory = $orderFactory;
        $this->orderRepository = $orderRepository;
        $this->cartRepository = $cartRepository;
        $this->transactionFactory = $transactionFactory;
        $this->request = $request;
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->storeIdentifierChecker = $storeIdentifierChecker;
        $this->orderIdentity = $orderIdentity;
        $this->orderSender = $orderSender;
        $this->invoiceIdentity = $invoiceIdentity;
        $this->invoiceSender = $invoiceSender;
        $this->logger = $logger;
    }

    /**
     * @return string|null
     */
    private function getNotificationType(): ?string
    {
        return $this->notificationType;
    }

    /**
     * @param string|null $notificationType
     * @return void
     */
    private function setNotificationType(string $notificationType = null): void
    {
        $this->notificationType = $notificationType;
    }

    /**
     * @return bool
     */
    private function getIsTestWebhook(): bool
    {
        return $this->isTestWebhook;
    }

    /**
     * @param bool $value
     * @return void
     */
    private function setIsTestWebhook(bool $value): void
    {
        $this->isTestWebhook = $value;
    }

    /**
     * @return int|null
     */
    private function getQuiteId(): ?int
    {
        return $this->quoteId;
    }

    /**
     * @param array $payload
     * @param int|null $quoteId
     * @return void
     */
    private function setQuiteId(array $payload = [], int $quoteId = null): void
    {
        if ((null === $quoteId) && !empty($payload)) {
            $customParameters = $payload[self::PAYLOAD_KEY_CUSTOM_PARAMETERS] ?? [];
            if (!empty($customParameters) && isset($customParameters[CustomParameterDataBuilder::QUOTE_ID])) {
                $quoteId = (int)$customParameters[CustomParameterDataBuilder::QUOTE_ID];
            }
        }
        $this->quoteId = $quoteId;
    }

    /**
     * @return bool
     */
    public function getIsStoreRelevant(): bool
    {
        return $this->isStoreRelevant;
    }

    /**
     * @param bool $value
     * @return void
     */
    private function setIsStoreRelevant(bool $value): void
    {
        $this->isStoreRelevant = $value;
    }

    /**
     * Decrypt payment data
     *
     * @param int $storeId
     * @param string $paymentData
     * @return string|null
     */
    private function decryptPaymentData(int $storeId, string $paymentData): ?string
    {
        $encryptionSecret = $this->config->getWebhooksEncryptionSecret($storeId);
        $initializationVector = $this->request->getHeader(self::HEADER_X_INITIALIZATION_VECTOR, null);
        $authenticationTag = $this->request->getHeader(self::HEADER_X_AUTHENTICATION_TAG, null);

        if ($encryptionSecret && $initializationVector && $authenticationTag) {
            $data = hex2bin($paymentData);
            $passphrase = hex2bin($encryptionSecret);
            $iv = hex2bin($initializationVector);
            $tag = hex2bin($authenticationTag);

            return openssl_decrypt(
                $data,
                self::OPENSSL_DECRYPT_CIPHER_ALGO,
                $passphrase,
                OPENSSL_RAW_DATA,
                $iv,
                $tag
            );
        }

        return null;
    }

    /**
     * Validate payment data
     *
     * @param array $paymentData
     * @return void
     * @throws WebhookException
     */
    private function validatePaymentData(array $paymentData = []): void
    {
        $type = $paymentData['type'] ?? null;
        $payload = $paymentData['payload'] ?? [];
        if (empty($payload)) {
            $message = __('WEBHOOK: Empty payload. Among others, please check the Encryption Secret.');
            $this->logger->critical($message, []);
            throw new WebhookException($message);
        }

        $this->setNotificationType($type);

        if ($type == self::NOTIFICATION_TYPE_TEST) {
            $message = __('WEBHOOK: ' . $payload['result']['description']);
            $this->logger->notice($message, []);
            // dirty way, no other way to display message
            echo $message;

            $this->setIsTestWebhook(true);
            return;
        }
        if (!in_array($type, [self::NOTIFICATION_TYPE_PAYMENT, self::NOTIFICATION_TYPE_REGISTRATION])) {
            $message = __('WEBHOOK: Invalid webhook type. Type %1 is not valid.', $type);
            $this->logger->critical($message, []);
            throw new WebhookException($message);
        }
    }

    /**
     * Prepare payload
     *
     * @param $storeId
     * @param $paymentData
     * @param bool $delayedUpdate
     * @return array
     * @throws WebhookException
     */
    private function preparePayload($storeId, $paymentData = null, bool $delayedUpdate = true): array
    {
        if (null === $paymentData) {
            $paymentData = $this->request->getContent();
        }

        $payload = $paymentData;
        if ($delayedUpdate) {
            $payload = [];
            if ($decryptedPaymentData = $this->decryptPaymentData($storeId, $paymentData)) {
                $decryptedPaymentData = $this->serializer->unserialize($decryptedPaymentData);
                $this->validatePaymentData($decryptedPaymentData);
                $payload = (array)$decryptedPaymentData[self::PAYLOAD];
            }
        }

        return (array)$payload;
    }

    /**
     * Init order object based on the payload data
     *
     * @param array $payload
     * @return OrderInterface
     * @throws WebhookException
     */
    private function initOrderFromPayload(array $payload): OrderInterface
    {
        $merchantTransactionId = $payload['merchantTransactionId'] ?? null;
        if (!$merchantTransactionId) {
            $message = __('WEBHOOK: Merchant transaction ID is not specified.');
            $this->logger->critical($message, []);
            throw new WebhookException($message);
        }

        /** @var OrderInterface $order */
        $order = $this->orderFactory->create()->loadByIncrementId($merchantTransactionId);
        if (!$orderId = $order->getId()) {
            $message = __('WEBHOOK: Order with the increment ID %1 doesn\'t exist', $merchantTransactionId);
            $this->logger->critical($message, []);
            throw new WebhookException($message);
        }

        $isWebhooksUpdateRequired = $order->getOppWebhooksUpdateRequired();
        if (!$isWebhooksUpdateRequired && ($order->getState() != Order::STATE_PENDING_PAYMENT)) {
            // already processed/processing?
            return $order;
        }

        $customParameters = $payload[self::PAYLOAD_KEY_CUSTOM_PARAMETERS] ?? [];
        if (empty($customParameters) && !$isWebhooksUpdateRequired) {
            $message = __('WEBHOOK: Order with the ID %1 doesn\'t intended for webhook processing', $orderId);
            $this->logger->critical($message, []);
            throw new WebhookException($message);
        }

        $webhookRequired = $customParameters[CustomParameterDataBuilder::WEBHOOKS_UPDATE_REQUIRED];
        if (!$isWebhooksUpdateRequired || !$webhookRequired) {
            $message = __('WEBHOOK: Order with the ID %1 doesn\'t intended for webhook processing', $orderId);
            $this->logger->critical($message, []);
            throw new WebhookException($message);
        }

        return $order;
    }

    /**
     * Apply order transaction details
     *
     * @param Payment $payment
     * @param Order $order
     * @param array $payload
     * @return void
     * @throws CommandException
     * @throws LocalizedException
     * @throws NotFoundException
     */
    private function executeTransaction(Payment $payment, Order $order, array $payload): void
    {
        $this->logger->debug("WEBHOOK: Before apply transaction details.", $payload);
        try {
            /** @var TransactionDetailsHandler $transactionDetailsHandler */
            $transactionDetailsHandler = $this->transactionDetailsHandlerFactory->create(['response' => $payload]);
            $transactionDetailsHandler->execute($payment, $order);
        } catch (\Exception $e) {
            $message = __('WEBHOOK: Apply transaction details error: ' . $e->getMessage());
            $this->logger->critical($message, []);
            throw new CommandException($message);
        }
        $this->logger->debug("WEBHOOK: After apply transaction details.");
    }

    /**
     * Apply order capture
     *
     * @param Payment $payment
     * @param Order $order
     * @return void
     * @throws LocalizedException|\Exception
     */
    private function executeCapture(Payment $payment, Order $order): void
    {
        try {
            $totalDue = $order->getTotalDue();
            $baseTotalDue = $order->getBaseTotalDue();

            $payment->setAmountAuthorized($totalDue);
            $payment->setBaseAmountAuthorized($baseTotalDue);
            $payment->capture();

            $order->setCustomerNote(__('Debit payment action by OPP. Processed by webhook.'));
            $order->setState(Order::STATE_PROCESSING);
            $order->setStatus($order->getConfig()->getStateDefaultStatus(Order::STATE_PROCESSING));
            $order->setIsNotified(false);
        } catch (\Exception $e) {
            $message = 'WEBHOOK: Order capture error: ' . $e->getMessage();
            $this->logger->critical(__($message), []);
            throw new \Exception($message);
        }
    }

    /**
     * Cancel pending order due to the payment transaction error
     *
     * @param array $payload
     * @return void
     * @throws WebhookException
     * @throws \Exception
     */
    private function cancelOrder(array $payload): void
    {
        $order = $this->initOrderFromPayload($payload);
        $orderId = $order->getId();

        $this->logger->debug("WEBHOOK: Before cancel order with ID {$orderId}");

        if (!$this->isOrderStateValidForCancel($order)) {
            throw new WebhookException('WEBHOOK: Order state is not valid');
        }

        try {
            $message = 'Order cancel reason: ' . $payload['result']['description'];
            $order->setCustomerNote(__($message));
            $order->setOppWebhooksUpdateRequired(false);
            $order->registerCancellation($message)->save();
            $this->orderRepository->save($order);
            $this->logger->info(__('WEBHOOK: ' . $message), []);
            $this->logger->info(__('WEBHOOK: Order with ID %1 was processed successfully.', $orderId), []);
        } catch (\Exception $e) {
            $message = 'WEBHOOK: Order cancel error: ' . $e->getMessage();
            $this->logger->critical(__($message), []);
            throw new \Exception($message);
        }

        $this->reset(false);

        $this->logger->debug("WEBHOOK: After cancel order.");
    }

    /**
     * Whether the order state is valid before capture
     *
     * @param Order $order
     * @return bool
     */
    private function isOrderStateValidForCapture(Order $order): bool
    {
        $orderId = $order->getId();
        $state = $order->getState();

        if ($order->hasInvoices()) {
            if ($state == Order::STATE_COMPLETE) {
                $this->logger->info(
                    'WEBHOOK: Order with ID ' . $orderId . ' is already complete.'
                );
                return false;
            }
            if ($state == Order::STATE_CLOSED) {
                $this->logger->info(
                    'WEBHOOK: Order with ID ' . $orderId . ' is already closed.'
                );
                return false;
            }
            if ($state == Order::STATE_CANCELED) {
                $this->logger->info(
                    'WEBHOOK: Order with ID ' . $orderId . ' is already canceled.'
                );
                return false;
            }
            if ($state == Order::STATE_PROCESSING) {
                $this->logger->info(
                    'WEBHOOK: Order with ID ' . $orderId . ' is already in the processing.'
                );
                return false;
            }
        }

        if ($state != Order::STATE_PENDING_PAYMENT) {
            $this->logger->info(
                'WEBHOOK: Order with ID ' . $orderId . ' is not in the correct state for the further processing. 
                Current state - ' . $order->getStatus() . '. Expected state - ' . Order::STATE_PENDING_PAYMENT
            );
            return false;
        }

        return true;
    }

    /**
     * Whether the order state is valid before cancel
     *
     * @param Order $order
     * @return bool
     */
    private function isOrderStateValidForCancel(Order $order): bool
    {
        $orderId = $order->getId();
        $state = $order->getState();

        if ($state == Order::STATE_COMPLETE) {
            $this->logger->debug("WEBHOOK: Order with ID {$orderId} is already complete'");
            return false;
        }
        if ($state == Order::STATE_CLOSED) {
            $this->logger->debug("WEBHOOK: Order with ID {$orderId} is already closed'");
            return false;
        }
        if ($state == Order::STATE_CANCELED) {
            $this->logger->debug("WEBHOOK: Order with ID {$orderId} is already canceled'");
            return false;
        }

        return true;
    }

    /**
     * Send order emails if allowed.
     *
     * @param OrderInterface $order
     * @return void
     */
    private function sendEmail(OrderInterface $order): void
    {
        if (!$order->getCanSendNewEmailFlag()) {
            return;
        }

        $orderId = (int)$order->getId();

        if ($this->orderIdentity->isEnabled()) {
            try {
                $this->orderSender->send($order);
                $this->logger->debug("WEBHOOK: The order confirmation email was sent.");
            } catch (\Throwable $e) {
                $this->logger->debug(
                    "WEBHOOK: Can\'t send order confirmation email for order with ID {$orderId}. {$e->getMessage()}"
                );
            }
        } else {
            $this->logger->debug("WEBHOOK: Send order confirmation email is disabled by store configuration.");
        }

        if ($this->invoiceIdentity->isEnabled()) {
            try {
                $invoice = current($order->getInvoiceCollection()->getItems());
                if ($invoice) {
                    $this->invoiceSender->send($invoice);
                    $this->logger->debug("WEBHOOK: The invoice confirmation email was sent.");
                }
            } catch (\Throwable $e) {
                $this->logger->debug(
                    "WEBHOOK: Can\'t send invoice confirmation email for order with ID {$orderId}. {$e->getMessage()}"
                );
            }
        } else {
            $this->logger->debug("WEBHOOK: Send invoice confirmation email is disabled by store configuration.");
        }
    }

    /**
     * Capture order (create invoice, add transaction details, etc.)
     *
     * @param array $payload
     * @return void
     * @throws LocalizedException
     * @throws \Exception
     */
    private function captureOrder(array $payload): void
    {
        $order = $this->initOrderFromPayload($payload);
        $orderId = $order->getId();

        $this->logger->debug("WEBHOOK: Before capture order with ID {$orderId}");

        /** @var OrderPaymentInterface $payment */
        $payment = $order->getPayment();
        if (!$payment) {
            $message = __("WEBHOOK: Payment object for the order with ID {$orderId} doesn't exist.");
            $this->logger->critical($message, []);
            throw new WebhookException($message);
        }

        if ($this->isOrderStateValidForCapture($order)) {
            $this->executeTransaction($payment, $order, $payload);
            $this->executeCapture($payment, $order);
            $order->setOppWebhooksUpdateRequired(false);
        }

        $this->logger->debug("WEBHOOK: After capture order.");

        try {
            /** @var Transaction $transaction */
            $transaction = $this->transactionFactory->create();
            $transaction->addObject($payment)
                ->addObject($order);
            $transaction->save();

            $order->setCanSendNewEmailFlag(true);
            $this->sendEmail($order);
            $this->logger->info(__('WEBHOOK: Order with ID %1 was processed successfully.', $orderId), []);
        } catch (\Exception $e) {
            $message = 'WEBHOOK: Can\'t save order with ID ' . $orderId . ' . Error: ' . $e->getMessage();
            $this->logger->critical(__($message), []);
            throw new \Exception($message);
        }

        $this->reset();
    }

    /**
     * Set quote as inactive
     *
     * @return void
     * @throws \Exception
     */
    private function setInactiveQuote(): void
    {
        $quoteId = $this->getQuiteId();
        if (null === $quoteId) {
            return;
        }

        try {
            $quote = $this->cartRepository->getActive($quoteId);
            $quote->setIsActive(false);
            $this->cartRepository->save($quote);
        } catch (\Exception $e) {
            // omit exception - quote already inactive
        }
    }

    /**
     * Reset local cache
     *
     * @param bool $resetQuote
     * @return void
     * @throws \Exception
     */
    private function reset(bool $resetQuote = true): void
    {
        if ($resetQuote) {
            $this->setInactiveQuote();
        }
        $this->setQuiteId();
        $this->setNotificationType();
        $this->setIsTestWebhook(false);
        $this->setIsStoreRelevant(true);
    }

    /**
     * Whether the incoming webhook is directly related to the Magento store
     *
     * @param array $payload
     * @param int $storeId
     * @return bool
     * @throws NoSuchEntityException
     */
    private function isStoreRelevant(array $payload, int $storeId): bool
    {
        if (isset($payload[self::PAYLOAD_KEY_CUSTOM_PARAMETERS][CustomParameterDataBuilder::PLUGIN])) {
            $plugin = (string)$payload[self::PAYLOAD_KEY_CUSTOM_PARAMETERS][CustomParameterDataBuilder::PLUGIN];
            if (!preg_match('/^Magento\s+(.*?)\/\s+(.*?)TotalProcessing_Opp\s+(.*?)/', $plugin)) {
                $message = __('WEBHOOK: The webhook is not valid. The order was not made by Magento.');
                $this->logger->info($message, []);
                $this->setIsStoreRelevant(false);
                return false;
            }
        }

        if (isset($payload[self::PAYLOAD_KEY_CUSTOM_PARAMETERS][CustomParameterDataBuilder::WEBHOOKS_STORE_IDENTIFIER])) {
            $storeIdentifier = (string)$payload[self::PAYLOAD_KEY_CUSTOM_PARAMETERS][CustomParameterDataBuilder::WEBHOOKS_STORE_IDENTIFIER];
            if (!$this->storeIdentifierChecker->isStoreValid($storeIdentifier, $storeId)) {
                $message = __(
                    'WEBHOOK: The webhook is not valid. The order is not relevant and doesn\'t belong to the current store.'
                );
                $this->logger->info($message, []);
                $this->setIsStoreRelevant(false);
                return false;
            }
        }

        return true;
    }

    /**
     * Whether the transaction is success
     *
     * @param string $resultCode
     * @return bool
     */
    private function isTransactionSuccess(string $resultCode): bool
    {
        return (bool)preg_match('/^(000.000.|000.100.1|000.[36]|000.400.[1][12]0)/', $resultCode);
    }

    /**
     * Whether the transaction in the pending state
     *
     * @param string $resultCode
     * @return bool
     */
    private function isTransactionPending(string $resultCode): bool
    {
        return (bool)preg_match('/^(000\.200)/', $resultCode);
    }

    /**
     * @param array $payload
     * @param int|null $quoteId
     * @return void
     * @throws LocalizedException
     * @throws WebhookException
     * @throws \Exception
     */
    private function doAction(array $payload, int $quoteId = null): void
    {
        if ($this->getIsTestWebhook()) {
            $this->reset();
            return;
        }

        if ($this->getNotificationType() == self::NOTIFICATION_TYPE_REGISTRATION) {
            // the registration will be processed at the transaction processing stage
            $this->reset();
            return;
        }

        $paymentType = $payload[self::PAYLOAD_KEY_PAYMENT_TYPE] ?? null;
        if ($paymentType == self::PAYLOAD_PAYMENT_TYPE_REFUND) {
            // refund is already processed at this point
            $this->reset();
            return;
        }

        $this->setQuiteId($payload, $quoteId);

        $resultCode = $payload['result']['code'];
        if ($this->isTransactionSuccess($resultCode)) {
            $this->captureOrder($payload);
            return;
        } elseif ($this->isTransactionPending($resultCode)) {
            // do nothing, waiting for confirmation
            $this->logger->info(__('WEBHOOK: Transaction is pending. Waiting for confirmation.'), []);
            return;
        }

        $this->cancelOrder($payload);
    }

    /**
     * @inheriDoc
     */
    public function update($paymentData = null, bool $delayedUpdate = true, int $quoteId = null): void
    {
        $storeId = (int)$this->storeManager->getStore()->getId();
        if (!$this->config->isWebhooksAvailable($storeId)) {
            $message = __('WEBHOOK: The webhook is not available.');
            $this->logger->info($message, []);
            throw new WebhookException($message);
        }

        $payload = $this->preparePayload($storeId, $paymentData, $delayedUpdate);

        if (!$this->isStoreRelevant($payload, $storeId)) {
            return;
        }

        $this->doAction($payload);
    }
}