<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model\Webhook\Order;

use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class CancelService
 * @package TotalProcessing\Opp\Model
 */
class StoreIdentifierChecker
{
    const HASH_PREFIX = 'TP_OPP';

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * @param StoreManagerInterface $storeManager
     * @param EncryptorInterface $encryptor
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        EncryptorInterface $encryptor
    ) {
        $this->storeManager = $storeManager;
        $this->encryptor = $encryptor;
    }

    /**
     * @param $storeId
     * @return string
     * @throws NoSuchEntityException
     */
    private function getStoreBaseUrl($storeId = null): string
    {
        return (string)$this->storeManager->getStore($storeId)->getBaseUrl();
    }

    /**
     * @param string $storeBaseUrl
     * @return string
     */
    private function hash(string $storeBaseUrl): string
    {
        return $this->encryptor->hash($storeBaseUrl);
    }

    /**
     * @param null $storeId
     * @param bool $useHash
     * @return string
     * @throws NoSuchEntityException
     */
    public function getStoreIdentifier($storeId = null, bool $useHash = false): string
    {
        $identifier = (string)$this->getStoreBaseUrl($storeId);
        if ($useHash) {
            $identifier = sprintf('%s_%s', self::HASH_PREFIX, $this->hash($identifier));
        }

        return $identifier;
    }

    /**
     * @param string $contextIdentifier
     * @param null $storeId
     * @param bool $useHash
     * @return bool
     * @throws NoSuchEntityException
     */
    public function isStoreValid(string $contextIdentifier, $storeId = null, bool $useHash = false): bool
    {
        $identifier = (string)$this->getStoreBaseUrl($storeId);
        if ($useHash) {
            $identifier = sprintf('%s_%s', self::HASH_PREFIX, $this->hash($identifier));
        }

         return $contextIdentifier === $identifier;
    }
}
