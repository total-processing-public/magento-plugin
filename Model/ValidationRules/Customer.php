<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model\ValidationRules;

use Magento\Backend\Model\Session\Quote as Session;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Framework\Validation\ValidationResultFactory;
use Magento\Store\Model\ScopeInterface;

/**
 * @inheritdoc
 */
class Customer extends AbstractValidationRule implements PaymentValidationRuleInterface
{
    /**
     * @var CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var ValidationResultFactory
     */
    private $validationResultFactory;

    /**
     * @var string
     */
    private $generalMessage;

    /**
     * @param Session $session
     * @param CustomerRepositoryInterface $customerRepository
     * @param ValidationResultFactory $validationResultFactory
     * @param string $generalMessage
     */
    public function __construct(
        Session $session,
        CustomerRepositoryInterface $customerRepository,
        ValidationResultFactory $validationResultFactory,
        string $generalMessage = ''
    ) {
        parent::__construct($session);
        $this->customerRepository = $customerRepository;
        $this->validationResultFactory = $validationResultFactory;
        $this->generalMessage = $generalMessage;
    }

    /**
     * @return bool
     */
    private function isValid(): bool
    {
        if (!$customerEmail = $this->getQuote()->getCustomerEmail()) {
            return false;
        }

        $customer = $this->getQuote()->getCustomer();
        if ($customer->getId()) {
            return true;
        }

        $customer = null;
        try {
            $websiteId = $this->getQuote()->getStore()->getWebsiteId() ?? null;
            $customer = $this->customerRepository->get($customerEmail, $websiteId);
        } catch (\Exception $e) {
            return true;
        }

        return !$customer->getId();
    }

    /**
     * @inheritdoc
     */
    public function validate(bool $beforeRender = false): array
    {
        $validationErrors = [];
        if ($beforeRender) {
            return [$this->validationResultFactory->create(['errors' => $validationErrors])];
        }

        $validationResult = $this->isValid();
        if (!$validationResult) {
            $validationErrors = [__($this->generalMessage)];
        }

        return [$this->validationResultFactory->create(['errors' => $validationErrors])];
    }
}
