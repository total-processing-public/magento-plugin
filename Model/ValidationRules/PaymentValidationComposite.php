<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model\ValidationRules;

/**
 * @inheritdoc
 */
class PaymentValidationComposite implements PaymentValidationRuleInterface
{
    /**
     * @var PaymentValidationRuleInterface[]
     */
    private $validationRules = [];

    /**
     * @param array $validationRules
     */
    public function __construct(array $validationRules) {
        foreach ($validationRules as $validationRule) {
            if (!($validationRule instanceof PaymentValidationRuleInterface)) {
                throw new \InvalidArgumentException(
                    sprintf(
                        'Instance of the PaymentValidationRuleInterface is expected, got %s instead.',
                        get_class($validationRule)
                    )
                );
            }
        }
        $this->validationRules = $validationRules;
    }

    /**
     * @inheritdoc
     */
    public function validate(bool $beforeRender = false): array
    {
        $aggregateResult = [];
        foreach ($this->validationRules as $validationRule) {
            $ruleValidationResult = $validationRule->validate($beforeRender);
            foreach ($ruleValidationResult as $item) {
                if (!$item->isValid()) {
                    array_push($aggregateResult, $item);
                }
            }
        }

        return $aggregateResult;
    }
}
