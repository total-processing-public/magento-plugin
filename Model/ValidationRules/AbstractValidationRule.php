<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model\ValidationRules;

use Magento\Backend\Model\Session\Quote as Session;
use Magento\Quote\Model\Quote;

/**
 * Class AbstractValidationRule
 * @package TotalProcessing\Opp\Model\ValidationRules
 */
abstract class AbstractValidationRule
{
    /**
     * Quote session object
     *
     * @var Session
     */
    private $session;

    /**
     * Quote associated with the model
     *
     * @var Quote
     */
    private $quote;

    /**
     * @param Session $session
     */
    public function __construct(
        Session $session
    ) {
        $this->session = $session;
    }

    /**
     * Retrieve quote object model
     *
     * @return Quote
     */
    protected function getQuote(): Quote
    {
        if (!$this->quote) {
            $this->quote = $this->session->getQuote();
        }
        return $this->quote;
    }
}
