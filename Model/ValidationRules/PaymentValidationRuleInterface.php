<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model\ValidationRules;

use Magento\Framework\Validation\ValidationResult;

/**
 * Interface PaymentValidationRuleInterface
 * @package TotalProcessing\Opp\Model\ValidationRules
 */
interface PaymentValidationRuleInterface
{
    /**
     * Validate payment before submit.
     *
     * @param bool $beforeRender
     * @return ValidationResult[]
     */
    public function validate(bool $beforeRender = false): array;
}
