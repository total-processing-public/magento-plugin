<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Model\ValidationRules;

use Magento\Backend\Model\Session\Quote as Session;
use Magento\Framework\Validation\ValidationResultFactory;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Model\System\Config\PaymentAction;

/**
 * @inheritdoc
 */
class PaymentConfig extends AbstractValidationRule implements PaymentValidationRuleInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var ValidationResultFactory
     */
    private $validationResultFactory;

    /**
     * @var string
     */
    private $generalMessage;

    /**
     * @var string
     */
    private $additionalMessage;

    /**
     * @param Session $session
     * @param Config $confi
     * @param ValidationResultFactory $validationResultFactory
     * @param string $generalMessage
     * @param string $additionalMessage
     */
    public function __construct(
        Session $session,
        Config $confi,
        ValidationResultFactory $validationResultFactory,
        string $generalMessage = '',
        string $additionalMessage = ''
    ) {
        parent::__construct($session);
        $this->config = $confi;
        $this->validationResultFactory = $validationResultFactory;
        $this->generalMessage = $generalMessage;
        $this->additionalMessage = $additionalMessage;
    }

    /**
     * @inheritdoc
     */
    public function validate(bool $beforeRender = true): array
    {
        $validationErrors = [];
        if (!$beforeRender) {
            return [$this->validationResultFactory->create(['errors' => $validationErrors])];
        }

        $message = '';
        $validationResult = true;
        $storeId = $this->getQuote()->getStoreId();
        if (!$this->config->getBackendEntityId($storeId) || !$this->config->getAccessToken($storeId)) {
            $message = $this->generalMessage;
            $validationResult = false;
        }
        $paymentAction = $this->config->getPaymentAction($storeId);
        if ($paymentAction != PaymentAction::DEBIT) {
            $additionalMessage = sprintf($this->additionalMessage, $paymentAction);
            if (strlen($message)) {
                $message .= '<br/><br/>' . $additionalMessage;
            } else {
                $message .= $additionalMessage;
            }
            $validationResult = false;
        }

        if (!$validationResult) {
            $validationErrors = [__($message)];
        }

        return [$this->validationResultFactory->create(['errors' => $validationErrors])];
    }
}
