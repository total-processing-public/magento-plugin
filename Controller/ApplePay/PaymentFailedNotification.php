<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\ApplePay;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Checkout\Helper\Data as CheckoutHelperData;
use Magento\Framework\Session\SessionManagerInterface;

/**
 * Class PaymentFailedNotification
 * @package TotalProcessing\Opp\Controller\ApplePay
 */
class PaymentFailedNotification extends Action implements HttpPostActionInterface
{
    /**
     * @var SessionManagerInterface
     */
    private $checkoutSession;

    /**
     * @var CheckoutHelperData
     */
    private $checkoutHelperData;

    /**
     * @param Context $context
     * @param SessionManagerInterface $checkoutSession
     * @param CheckoutHelperData $checkoutHelperData
     */
    public function __construct(
        Context $context,
        SessionManagerInterface $checkoutSession,
        CheckoutHelperData $checkoutHelperData
    ) {
        parent::__construct($context);
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelperData = $checkoutHelperData;
    }

    /**
     * @return void
     */
    public function execute()
    {
        if ($message = $this->getRequest()->getParam('message')) {
            try {
                $this->checkoutHelperData->sendPaymentFailedEmail($this->checkoutSession->getQuote(), $message);
            } catch (\Exception $e) {
                // omit exception
            }
        }
    }
}
