<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Webhook\Order;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\Exception\LocalizedException;
use TotalProcessing\Opp\Api\WebhookOrderProcessorInterface;
use TotalProcessing\Opp\Exception\WebhookException;

/**
 * Class Update
 * @package TotalProcessing\Opp\Controller\Webhook\Order
 */
class Update extends Action implements CsrfAwareActionInterface
{
    const RESPONSE_CODE_ACCEPTED = 202;

    /**
     * @var WebhookOrderProcessorInterface
     */
    private $webhookOrderProcessor;

    /**
     * @param Context $context
     * @param WebhookOrderProcessorInterface $webhookOrderProcessor
     */
    public function __construct(
        Context $context,
        WebhookOrderProcessorInterface $webhookOrderProcessor
    ) {
        parent::__construct($context);
        $this->webhookOrderProcessor = $webhookOrderProcessor;
    }

    /**
     * @return void
     * @throws WebhookException
     */
    private function validateRequest(): void
    {
        if ($this->getRequest()->getMethod() !== 'POST') {
            throw new WebhookException(__("Invalid payment webhook HTTP method."));
        }

        if (!$this->getRequest()->getContent()) {
            throw new WebhookException(__("Invalid payment webhook POST data."));
        }
    }

    /**
     * @return void
     * @throws WebhookException
     * @throws LocalizedException
     */
    public function execute()
    {
        $this->validateRequest();
        try {
            $this->webhookOrderProcessor->update($this->getRequest()->getContent());
            if (!$this->webhookOrderProcessor->getIsStoreRelevant()) {
                $this->getResponse()->setHttpResponseCode(self::RESPONSE_CODE_ACCEPTED);
            }
        } catch (WebhookException $e) {
            throw new WebhookException(__('Payment webhook order processor error. ' . $e->getMessage()));
        }
    }

    /**
     * @param RequestInterface $request
     * @return InvalidRequestException|null
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @param RequestInterface $request
     * @return bool|null
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}