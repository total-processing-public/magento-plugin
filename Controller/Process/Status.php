<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Process;

use Magento\Checkout\Helper\Data as CheckoutHelperData;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Quote\Api\CartRepositoryInterface;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Api\WebhookOrderProcessorInterface;
use TotalProcessing\Opp\Exception\WebhookException;
use TotalProcessing\Opp\Gateway\Helper\PaymentStatusResolver;
use TotalProcessing\Opp\Helper\Checkout as CheckoutHelper;

/**
 * Class Status
 * @package TotalProcessing\Opp\Controller\Process
 */
class Status extends BaseAction
{
    /**
     * @var PaymentStatusResolver
     */
    private $paymentStatusResolver;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var CheckoutHelperData
     */
    private $checkoutHelperData;

    /**
     * @var WebhookOrderProcessorInterface
     */
    private $webhookOrderProcessor;

    /**
     * @var CheckoutHelper
     */
    private $checkoutHelper;

    /**
     * @param Context $context
     * @param LoggerInterface $logger
     * @param SessionManagerInterface $checkoutSession
     * @param PaymentStatusResolver $paymentStatusResolver
     * @param CartRepositoryInterface $cartRepository
     * @param CheckoutHelperData $checkoutHelperData
     * @param WebhookOrderProcessorInterface $webhookOrderProcessor
     * @param CheckoutHelper $checkoutHelper
     */
    public function __construct(
        Context $context,
        LoggerInterface $logger,
        SessionManagerInterface $checkoutSession,
        PaymentStatusResolver $paymentStatusResolver,
        CartRepositoryInterface $cartRepository,
        CheckoutHelperData $checkoutHelperData,
        WebhookOrderProcessorInterface $webhookOrderProcessor,
        CheckoutHelper $checkoutHelper
    ) {
        parent::__construct(
            $context,
            $logger,
            $checkoutSession
        );
        $this->paymentStatusResolver = $paymentStatusResolver;
        $this->cartRepository = $cartRepository;
        $this->checkoutHelperData = $checkoutHelperData;
        $this->webhookOrderProcessor = $webhookOrderProcessor;
        $this->checkoutHelper = $checkoutHelper;
    }

    /**
     * @return void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        try {
            $this->paymentStatusResolver->resolve();
        } catch (CommandException $e) {
            $this->processError($e);
            return;
        }

        $this->_view->loadLayout();
        $layout = $this->_view->getLayout();
        $block = $layout->getBlock("opp_process_status");

        echo $block->toHtml();
    }

    /**
     * @param $exception
     * @return void
     * @throws LocalizedException
     * @throws WebhookException
     */
    private function processError($exception)
    {
        $message = $exception->getMessage();
        $this->logger->critical($message);
        if ($quoteId = $this->getRequest()->getParam('quote_id')) {
            try {
                $quote = $this->cartRepository->get($quoteId);
                if ($this->getRequest()->getParam('restore_quote_on_error')) {
                    // restores quote on order submit error
                    $this->checkoutHelper->restoreQuoteOnError($quote->getReservedOrderId());
                }
                $this->checkoutHelperData->sendPaymentFailedEmail($quote, $message);
            } catch (\Exception $e) {
                // omit exception
            }
        }

        $this->_forward('error', null, null, ['error_message' => $message]);
    }
}
