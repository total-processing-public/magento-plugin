<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Adminhtml\Transparent;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\Layout;
use TotalProcessing\Opp\Controller\Adminhtml\Payment\AbstractPayment;

/**
 * Class GatewayRedirect
 * @package TotalProcessing\Opp\Controller\Adminhtml\Transparent
 */
class GatewayRedirect extends AbstractPayment implements HttpGetActionInterface
{
    /**
     * @return Layout|ResultInterface
     */
    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->addDefaultHandle();

        return $resultLayout;
    }
}
