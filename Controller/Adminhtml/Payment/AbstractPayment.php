<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Adminhtml\Payment;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\View\Result\LayoutFactory;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Model\PaymentValidatorFactory;

/**
 * Class AbstractPayment
 * @package TotalProcessing\Opp\Controller\Adminhtml\Payment
 */
abstract class AbstractPayment extends Action
{
    /**
     * @var RawFactory
     */
    protected $resultRawFactory;

    /**
     * @var LayoutFactory
     */
    protected $resultLayoutFactory;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var PaymentValidatorFactory
     */
    protected $paymentValidatorFactory;

    /**
     * @param Context $context
     * @param RawFactory $resultRawFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param LoggerInterface $logger
     * @param PaymentValidatorFactory $paymentValidatorFactory
     */
    public function __construct(
        Context $context,
        RawFactory $resultRawFactory,
        LayoutFactory $resultLayoutFactory,
        LoggerInterface $logger,
        PaymentValidatorFactory $paymentValidatorFactory
    ) {
        parent::__construct($context);
        $this->resultRawFactory = $resultRawFactory;
        $this->resultLayoutFactory = $resultLayoutFactory;
        $this->logger = $logger;
        $this->paymentValidatorFactory = $paymentValidatorFactory;
    }
}
