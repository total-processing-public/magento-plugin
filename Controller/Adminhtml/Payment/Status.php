<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Adminhtml\Payment;

use Magento\Checkout\Helper\Data as CheckoutHelperData;
use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Quote\Api\CartRepositoryInterface;
use Psr\Log\LoggerInterface;
use TotalProcessing\Opp\Gateway\Helper\Backend\PaymentStatusResolver;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\View\Result\LayoutFactory;
use Magento\Framework\FlagManager;
use TotalProcessing\Opp\Gateway\Helper\Backend\FlagProvider;
use TotalProcessing\Opp\Model\PaymentValidatorFactory;

/**
 * Class Status
 * @package TotalProcessing\Opp\Controller\Adminhtml\Payment
 */
class Status extends AbstractPayment implements HttpGetActionInterface
{
    /**
     * @var FlagManager
     */
    private $flagManager;

    /**
     * @var PaymentStatusResolver
     */
    private $paymentStatusResolver;

    /**
     * @var CartRepositoryInterface
     */
    private $cartRepository;

    /**
     * @var CheckoutHelperData
     */
    private $checkoutHelperData;

    /**
     * @param Context $context
     * @param RawFactory $resultRawFactory
     * @param LayoutFactory $resultLayoutFactory
     * @param LoggerInterface $logger
     * @param PaymentValidatorFactory $paymentValidatorFactory
     * @param FlagManager $flagManager
     * @param PaymentStatusResolver $paymentStatusResolver
     * @param CartRepositoryInterface $cartRepository
     * @param CheckoutHelperData $checkoutHelperData
     */
    public function __construct(
        Context $context,
        RawFactory $resultRawFactory,
        LayoutFactory $resultLayoutFactory,
        LoggerInterface $logger,
        PaymentValidatorFactory $paymentValidatorFactory,
        FlagManager $flagManager,
        PaymentStatusResolver $paymentStatusResolver,
        CartRepositoryInterface $cartRepository,
        CheckoutHelperData $checkoutHelperData
    ) {
        parent::__construct(
            $context,
            $resultRawFactory,
            $resultLayoutFactory,
            $logger,
            $paymentValidatorFactory
        );
        $this->flagManager = $flagManager;
        $this->paymentStatusResolver = $paymentStatusResolver;
        $this->cartRepository = $cartRepository;
        $this->checkoutHelperData = $checkoutHelperData;
    }

    /**
     * @return ResultInterface|void
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function execute()
    {
        // The flag must be removed to prevent any re-directs or unexpected behavior.
        // @see \TotalProcessing\Opp\Plugin\Backend\App\AbstractAction::beforeDispatch()
        $this->flagManager->deleteFlag(FlagProvider::REDIRECT_SESSION_ID);

        try {
            $this->paymentStatusResolver->resolve();
        } catch (CommandException $e) {
            $this->processError($e);
            return;
        }

        $this->flagManager->deleteFlag(FlagProvider::REDIRECT_QUOTE_ID);
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->addDefaultHandle();
        $resultLayout->getLayout()->getUpdate()->load(['totalprocessing_opp_payment_status']);

        return $resultLayout;
    }

    /**
     * @param $exception
     * @return void
     */
    private function processError($exception)
    {
        $message = $exception->getMessage();
        $this->logger->critical($message);
        try {
            $quote = $this->cartRepository->get(
                $this->flagManager->getFlagData(FlagProvider::REDIRECT_QUOTE_ID)
            );
            $this->checkoutHelperData->sendPaymentFailedEmail($quote, $message, 'backend');
        } catch (\Exception $e) {
            // omit exception
        }
        $this->flagManager->deleteFlag(FlagProvider::REDIRECT_QUOTE_ID);
        $this->_forward('error', null, null, ['error_message' => $message, 'reload' => true]);
    }
}
