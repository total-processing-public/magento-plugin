<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Adminhtml\Payment;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use TotalProcessing\Opp\Model\PaymentValidator;

/**
 * Class Iframe
 * @package TotalProcessing\Opp\Controller\Adminhtml\Payment
 */
class Iframe extends AbstractPayment implements HttpGetActionInterface
{
    /**
     * {@inheritdoc}
     */
    public function execute()
    {
        /** @var PaymentValidator $validator */
        $validator = $this->paymentValidatorFactory->create();
        $result = $validator->validate(true);
        if (!$result['isValid'] && isset($result['messages'])) {
            $this->_forward(
                'error',
                null,
                null,
                [
                    'error_message' => (string)$result['messages'],
                    'reload' => false
                ]);
            return;
        }

        $this->_view->loadLayout();
        $this->_view->getLayout()->initMessages();
        $this->_view->renderLayout();
    }
}
