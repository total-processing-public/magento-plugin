<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Adminhtml\Payment;

use Magento\Framework\App\Action\HttpPostActionInterface as HttpPostActionInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use TotalProcessing\Opp\Model\PaymentValidator;

/**
 * Class Error
 * @package TotalProcessing\Opp\Controller\Adminhtml\Payment
 */
class Validator extends AbstractPayment implements HttpPostActionInterface
{
    /**
     * @return Json
     */
    public function execute(): Json
    {
        /** @var PaymentValidator $validator */
        $validator = $this->paymentValidatorFactory->create();
        $data = [
            'validationResult' => $validator->validate()
        ];
        /** @var Json $resultJson */
        $resultJson = $this->resultFactory->create(ResultFactory::TYPE_JSON);
        $resultJson->setData($data);

        return $resultJson;
    }
}
