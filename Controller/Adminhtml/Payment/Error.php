<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Controller\Adminhtml\Payment;

use Magento\Framework\App\Action\HttpGetActionInterface as HttpGetActionInterface;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\AbstractBlock;

/**
 * Class Error
 * @package TotalProcessing\Opp\Controller\Adminhtml\Payment
 */
class Error extends AbstractPayment implements HttpGetActionInterface
{
    /**
     * @return ResultInterface
     * @throws LocalizedException
     */
    public function execute()
    {
        $resultLayout = $this->resultLayoutFactory->create();
        $resultLayout->addDefaultHandle();
        /** @var AbstractBlock $block */
        $block = $resultLayout->getLayout()->getBlock('opp_payment_error');
        if ($block) {
            $block->setData('error_message', $this->getRequest()->getParam('error_message'));
            $block->setData('reload', $this->getRequest()->getParam('reload'));
        }
        $resultLayout->getLayout()->getUpdate()->load(['totalprocessing_opp_payment_error']);

        return $resultLayout;
    }
}
