<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Helper;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Exception\LocalizedException;
use Magento\Sales\Model\Order;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Class Checkout
 * @package TotalProcessing\Opp\Helper
 */
class Checkout
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @param CheckoutSession $checkoutSession
     * @param OrderRepositoryInterface $orderRepository
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        OrderRepositoryInterface $orderRepository
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->orderRepository = $orderRepository;
    }

    /**
     * Cancel last placed order with specified comment message
     *
     * @param string $comment Comment appended to order history
     * @return bool True if order cancelled, false otherwise
     * @throws LocalizedException
     */
    public function cancelCurrentOrder(string $comment): bool
    {
        $order = $this->checkoutSession->getLastRealOrder();
        if ($order->getId() && $order->getState() != Order::STATE_CANCELED) {
            $order->registerCancellation($comment)->save();
            return true;
        }
        return false;
    }

    /**
     * Restores quote
     *
     * @return bool
     */
    public function restoreQuote()
    {
        $session = $this->checkoutSession;
		$lastRealOrderId = $session->getLastRealOrderId();
        $session->restoreQuote();
        $session->setLastRealOrderId($lastRealOrderId);
    }

    /**
     * Restores quote on order place error
     *
     * @param string|null $reservedOrderId
     * @param Order|null $order
     * @param string|null $message
     * @return void
     */
    public function restoreQuoteOnError(
        string $reservedOrderId = null,
        Order $order = null,
        string $message = null
    ): void {
        if ($reservedOrderId) {
            $this->checkoutSession->setLastRealOrderId($reservedOrderId);
        }
        $session = $this->checkoutSession;
        $session->restoreQuote();
        if ($order) {
            $session->setLastRealOrderId($order->getIncrementId());
            if ($message) {
                $order->addStatusHistoryComment($message);
            }
            $this->orderRepository->save($order);
        }
    }
}