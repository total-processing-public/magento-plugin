<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Helper;

use Magento\Framework\App\DeploymentConfig;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Component\ComponentRegistrar;
use Magento\Framework\Component\ComponentRegistrarInterface;
use Magento\Framework\Exception\FileSystemException;
use Magento\Framework\Filesystem\Directory\ReadFactory;
use Magento\Framework\Filesystem\Directory\ReadInterface;
use Magento\Framework\Module\ResourceInterface;
use Magento\Framework\Serialize\SerializerInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\App\ProductMetadataInterface;

/**
 * Class MagentoMetadata
 * @package TotalProcessing\Opp\Helper
 */
class Metadata extends AbstractHelper
{
    /**
     * Module composer filename
     */
    const COMPOSER_FILE_NAME = 'composer.json';

    /**
     * Module metadata keys
     */
    const MODULE_METADATA_KEY_NAME = 'name';
    const MODULE_METADATA_KEY_MODULE_NAME = 'module_name';
    const MODULE_METADATA_KEY_DESCRIPTION = 'description';
    const MODULE_METADATA_KEY_CONFIG = 'config';
    const MODULE_METADATA_KEY_REQUIRE = 'require';
    const MODULE_METADATA_KEY_LICENSE = 'license';
    const MODULE_METADATA_KEY_AUTOLOAD = 'autoload';
    const MODULE_METADATA_KEY_VERSION = 'version';
    const MODULE_METADATA_KEY_SETUP_VERSION = 'setup_version';

    /**
     * Application metadata keys
     */
    const APP_METADATA_KEY_VERSION = 'version';
    const APP_METADATA_KEY_EDITION = 'edition';
    const APP_METADATA_KEY_NAME = 'name';

    /**
     * @var DeploymentConfig
     */
    private $deploymentConfig;

    /**
     * @var ComponentRegistrarInterface
     */
    private $componentRegistrar;

    /**
     * @var ReadFactory
     */
    private $readFactory;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ResourceInterface
     */
    private $moduleResource;

    /**
     * @var ProductMetadataInterface
     */
    private $productMetadata;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var mixed|null
     */
    private $moduleMetadata = null;

    /**
     * @var mixed|null
     */
    private $appMetadata = null;

    /**
     * @param Context $context
     * @param DeploymentConfig $deploymentConfig
     * @param ComponentRegistrarInterface $componentRegistrar
     * @param ReadFactory $readFactory
     * @param SerializerInterface $serializer
     * @param ResourceInterface $moduleResource
     * @param ProductMetadataInterface $productMetadata
     * @param LoggerInterface $logger
     */
    public function __construct(
        Context $context,
        DeploymentConfig $deploymentConfig,
        ComponentRegistrarInterface $componentRegistrar,
        ReadFactory $readFactory,
        SerializerInterface $serializer,
        ResourceInterface $moduleResource,
        ProductMetadataInterface $productMetadata,
        LoggerInterface $logger
    ) {
        parent::__construct($context);
        $this->deploymentConfig = $deploymentConfig;
        $this->componentRegistrar = $componentRegistrar;
        $this->readFactory = $readFactory;
        $this->serializer = $serializer;
        $this->moduleResource = $moduleResource;
        $this->productMetadata = $productMetadata;
        $this->logger = $logger;
    }

    /**
     * @param string|null $moduleName
     * @param string|null $field
     * @param bool $shortly
     * @return array|mixed|null
     */
    public function getModuleMetadata(string $moduleName = null, string $field = null, bool $shortly = false)
    {
        if (null === $this->moduleMetadata) {
            if (null === $moduleName) {
                $moduleName = $this->_getModuleName();
            }
            $path = $this->componentRegistrar->getPath(ComponentRegistrar::MODULE, $moduleName);
            /** @var ReadInterface $directoryRead */
            $directoryRead = $this->readFactory->create($path);

            $composerData = [];
            try {
                $composerJsonData = $directoryRead->readFile(self::COMPOSER_FILE_NAME);
                $composerData = $this->serializer->unserialize($composerJsonData);
            } catch (FileSystemException $e) {
                $this->logger->error('Can\'t retrieve module metadata from composer. ' . $e->getMessage());
            } catch (\InvalidArgumentException $e) {
                $this->logger->error('Can\'t convert module metadata. ' . $e->getMessage());
            }

            $this->moduleMetadata = array_merge(
                $composerData,
                [
                    self::MODULE_METADATA_KEY_MODULE_NAME => $moduleName,
                    self::MODULE_METADATA_KEY_SETUP_VERSION => $this->moduleResource->getDataVersion($moduleName)
                ]
            );

            $this->moduleMetadata = $this->moduleMetadata[$field] ?? $this->moduleMetadata;
            if (!$field && $shortly) {
                $this->moduleMetadata = sprintf(
                    'Module %s v%s',
                    $moduleName,
                    $this->moduleMetadata[self::MODULE_METADATA_KEY_VERSION] ?? 'N/A'
                );
            }
        }

        return $this->moduleMetadata;
    }

    /**
     * @param string|null $field
     * @param bool $asString
     * @return mixed|string|string[]|null
     */
    public function getAppMetadata(string $field = null, bool $asString = false)
    {
        if (null === $this->appMetadata) {
            $version = (string)$this->productMetadata->getVersion();
            $edition = (string)$this->productMetadata->getEdition();
            $name = (string)$this->productMetadata->getName();

            $metaData = [
                self::APP_METADATA_KEY_VERSION => $version,
                self::APP_METADATA_KEY_EDITION => $edition,
                self::APP_METADATA_KEY_NAME => $name
            ];

            $this->appMetadata = $metaData[$field] ?? $metaData;
            if (!$field && $asString) {
                $this->appMetadata = sprintf('%s %s v%s', $name, $edition, $version);
            }
        }

        return $this->appMetadata;
    }

    /**
     * @param string $version
     * @param string $operator
     * @return bool|int
     */
    public function versionCompare(string $version, string $operator = '>=')
    {
        return version_compare($this->getAppMetadata(self::APP_METADATA_KEY_VERSION), $version, $operator);
    }
}
