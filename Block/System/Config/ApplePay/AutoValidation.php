<?php

/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Block\System\Config\ApplePay;

use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use TotalProcessing\Opp\Model\Ui\ConfigProvider;
use TotalProcessing\Opp\Gateway\Config\ApplePay\Config;
use TotalProcessing\Opp\Model\Ui\ApplePay\ConfigProvider as ApplePayConfigProvider;

/**
 * Class AutoValidation
 * @package TotalProcessing\Opp\Block\System\Config\ApplePay
 */
class AutoValidation extends Field
{
    /**
     * @var string
     */
    protected $_template = 'TotalProcessing_Opp::system/config/apple-pay/auto-validation.phtml';

    /**
     * {@inheritdoc}
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }

    /**
     * Returns active select id
     *
     * @return string
     */
    public function getActiveElementId(): string
    {
        return ApplePayConfigProvider::CODE . '_' . Config::KEY_ACTIVE;
    }

    /**
     * Environment field id
     *
     * @return string
     */
    public function getEnvironmentElementId(): string
    {
        return ApplePayConfigProvider::CODE . "_" . Config::KEY_ENVIRONMENT;
    }

    /**
     * Returns merchant identifier field id
     *
     * @return string
     */
    public function getMerchantIdentifierElementId(): string
    {
        return Config::KEY_MERCHANT_IDENTIFIER;
    }

    /**
     * Returns button id
     *
     * @return string
     */
    public function getRegisterButtonId(): string
    {
        return "register_merchant_btn";
    }

    /**
     * {@inheritdoc}
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();
        return parent::render($element);
    }
}
