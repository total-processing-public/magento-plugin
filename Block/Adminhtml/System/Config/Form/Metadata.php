<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Block\Adminhtml\System\Config\Form;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\Data\Form\Element\AbstractElement;
use TotalProcessing\Opp\Helper\Metadata as HelperMetadata;

/**
 * Class Metadata
 * @package TotalProcessing\Opp\Block\Adminhtml\System\Config\Form
 */
class Metadata extends Field
{
    /**
     * @var HelperMetadata
     */
    private $metadata;

    /**
     * @param Context $context
     * @param HelperMetadata $metadata
     * @param array $data
     */
    public function __construct(
        Context $context,
        HelperMetadata $metadata,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->metadata = $metadata;
    }

    /**
     * @return array|mixed|null
     */
    public function getMetadata()
    {
        return $this->metadata->getModuleMetadata();
    }

    /**
     * {@inheritdoc}
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('TotalProcessing_Opp::system/config/metadata.phtml');
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
