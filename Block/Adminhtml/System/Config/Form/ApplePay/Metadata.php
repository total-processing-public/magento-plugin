<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Block\Adminhtml\System\Config\Form\ApplePay;

use Magento\Backend\Block\Template\Context;
use Magento\Config\Block\System\Config\Form\Field;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\Element\AbstractElement;
use Magento\Framework\Exception\NoSuchEntityException;
use TotalProcessing\Opp\Gateway\Config\ApplePay\Config;

/**
 * Class Metadata
 * @package TotalProcessing\Opp\Block\Adminhtml\System\Config\Form\ApplePay
 */
class Metadata extends Field
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var array
     */
    private $validationResults = [];

    /**
     * @param Context $context
     * @param Config $config
     * @param RequestInterface $request
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $config,
        RequestInterface $request,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->request = $request;
    }

    /**
     * @param $store
     * @return array
     * @throws NoSuchEntityException
     */
    public function getValidationResults($store = null): array
    {
        $this->initValidation($store);
        return $this->validationResults;
    }

    /**
     * @param array $result
     * @return void
     */
    public function setValidationResults(array $result): void
    {
        $this->validationResults[] = $result;
    }

    /**
     * @param $store
     * @return void
     */
    private function validateAuthorizationCredentials($store = null): void
    {
        if (!$this->config->getEntityId($store)) {
            $this->setValidationResults(
                [
                    'status' => 'error',
                    'data-role' => 'apple-pay-entity-id-status',
                    'message' => __('Add <strong>Entity ID</strong>: This field in the config is blank,
please add it in <strong>\'Base Settings\'</strong> before attempting domain registration.')
                ]
            );
        }
        if (!$this->config->getAccessToken($store)) {
            $this->setValidationResults(
                [
                    'status' => 'error',
                    'data-role' => 'apple-pay-access-token-status',
                    'message' => __('Add <strong>Access Token</strong>: This field in the config is blank,
please add it in <strong>\'Base Settings\'</strong> before attempting domain registration.')
                ]
            );
        }
    }

    /**
     * @param $store
     * @return void
     */
    public function validateMerchantRegistration($store = null): void
    {
        if (!$this->config->getMerchantIdentifier($store)) {
            $this->setValidationResults(
                [
                    'status' => 'error',
                    'data-role' => 'apple-pay-merchant-registration-status',
                    'message' => __('The merchant is not registered. Complete the Domain Registration Process:
                    Make sure you\'ve successfully registered your domain through the process below.')
                ]
            );
        } else {
            $this->setValidationResults(
                [
                    'status' => 'success',
                    'data-role' => 'apple-pay-merchant-registration-status',
                    'message' => __('The merchant is registered: You can use this payment solution.')
                ]
            );
        }
    }

    /**
     * @param $store
     * @return void
     */
    public function initValidation($store = null): void
    {
        if (null === $store) {
            $store = $this->request->getParam('store', null);
        }
        $this->validateAuthorizationCredentials($store);
        $this->validateMerchantRegistration($store);
    }

    /**
     * {@inheritdoc}
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (!$this->getTemplate()) {
            $this->setTemplate('TotalProcessing_Opp::system/config/apple-pay/metadata.phtml');
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function render(AbstractElement $element)
    {
        $element->unsScope()->unsCanUseWebsiteValue()->unsCanUseDefaultValue();

        return parent::render($element);
    }

    /**
     * {@inheritdoc}
     */
    protected function _getElementHtml(AbstractElement $element)
    {
        return $this->_toHtml();
    }
}
