<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Block\Adminhtml\Order\Create\Payment;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;

/**
 * Class Iframe
 * @package TotalProcessing\Opp\Block\Adminhtml\Order\Create\Payment
 */
class Iframe extends Template
{
    /**
     * @return Iframe
     */
    protected function _prepareLayout()
    {
        $this->pageConfig->addPageAsset('jquery/jquery.min.js');
        return parent::_prepareLayout();
    }

    /**
     * @param $store
     * @return int
     * @throws NoSuchEntityException
     */
    public function getStoreId($store = null): int
    {
        return (int)$this->_storeManager->getStore($store)->getId();
    }
}
