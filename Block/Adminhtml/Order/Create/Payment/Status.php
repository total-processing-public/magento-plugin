<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Block\Adminhtml\Order\Create\Payment;

use Magento\Framework\View\Element\Template;

/**
 * Class Status
 * @package TotalProcessing\Opp\Block\Adminhtml\Order\Create\Payment
 */
class Status extends Template
{
    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }
}
