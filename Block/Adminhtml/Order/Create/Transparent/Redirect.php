<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Block\Adminhtml\Order\Create\Transparent;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Redirect block for register specific params in layout
 *
 * Class Redirect
 * @package TotalProcessing\Opp\Block\Adminhtml\Order\Create\Transparent
 */
class Redirect extends Template
{
    /**
     * Route path key to make redirect url.
     */
    private const ROUTE_PATH = 'routePath';

    /**
     * @var UrlInterface
     */
    private $url;

    /**
     * @param Context $context
     * @param UrlInterface $url
     * @param array $data
     */
    public function __construct(
        Context $context,
        UrlInterface $url,
        array $data = []
    ) {
        $this->url = $url;
        parent::__construct($context, $data);
    }

    /**
     * Returns url for redirect.
     *
     * @return string
     */
    public function getRedirectUrl(): string
    {
        return $this->url->getUrl($this->getData(self::ROUTE_PATH));
    }

    /**
     * Returns params to be redirected.
     *
     * Encodes invalid UTF-8 values to UTF-8 to prevent character escape error.
     * Some payment methods send data in merchant defined language encoding
     * which can be different from the system character encoding (UTF-8).
     *
     * @return array
     */
    public function getParams(): array
    {
        $params = [];
        foreach ($this->_request->getParams() as $name => $value) {
            if (!empty($value) && mb_detect_encoding($value, 'UTF-8', true) === false) {
                $value = utf8_encode($value);
            }
            $params[$name] = $value;
        }
        return $params;
    }
}
