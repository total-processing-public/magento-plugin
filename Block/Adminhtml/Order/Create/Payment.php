<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Block\Adminhtml\Order\Create;

use TotalProcessing\Opp\Model\Ui\ConfigProvider;
use TotalProcessing\Opp\Gateway\Config\Config as GatewayConfig;
use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Payment
 * @package TotalProcessing\Opp\Block\Adminhtml\Order\Create
 */
class Payment extends Template
{
    /**
     * @var ConfigProviderInterface
     */
    private $config;

    /**
     * @var GatewayConfig
     */
    private $gatewayConfig;

    /**
     * @param Context $context
     * @param ConfigProviderInterface $config
     * @param GatewayConfig $gatewayConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        ConfigProviderInterface $config,
        GatewayConfig $gatewayConfig,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->config = $config;
        $this->gatewayConfig = $gatewayConfig;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return ConfigProvider::CODE;
    }

    /**
     * @return string
     */
    public function getPaymentConfig(): string
    {
        $config = $this->config->getConfig();
        if (isset($config['payment'])) {
            $payment = $config['payment'];
            $config = $payment[$this->getCode()];
        }
        $config['code'] = $this->getCode();
        $config['renderSrc'] = $this->gatewayConfig->getBackendSource();
        $config['validationUrl'] = $this->getUrl('totalprocessing_opp/payment/validator');

        return json_encode($config, JSON_UNESCAPED_SLASHES);
    }
}
