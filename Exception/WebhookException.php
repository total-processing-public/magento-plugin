<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Exception;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class WebhookException
 * @package TotalProcessing\Opp\Exception
 */
class WebhookException extends LocalizedException
{
    /**
     * @var int
     */
    public $statusCode;

    /**
     * @param mixed $message
     * @param int $statusCode
     */
    public function __construct($message, $statusCode = 400)
    {
        $this->statusCode = $statusCode;
        if (is_string($message)) {
            parent::__construct(__($message));
        } else {
            parent::__construct($message);
        }
    }
}
