<?php
/**
 * Copyright © Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Plugin\Quote\Api;

use Magento\Quote\Api\CartManagementInterface as DefaultCartManagementInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\PaymentInterface;
use TotalProcessing\Opp\Model\Ui\ApplePay\ConfigProvider as ApplePayConfigProvider;
use TotalProcessing\Opp\Model\Ui\ConfigProvider as DefaultConfigProvider;
use TotalProcessing\Opp\Model\CancelServiceInterface;

/**
 * Class OrderCancellation
 * @package TotalProcessing\Opp\Plugin
 */
class CartManagementInterface
{
    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var CancelServiceInterface
     */
    private $cancelService;

    /**
     * @param CartRepositoryInterface $quoteRepository
     * @param CancelServiceInterface $cancelService
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        CancelServiceInterface $cancelService
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->cancelService = $cancelService;
    }

    /**
     * @param int $cartId
     * @param string|null $message
     * @return void
     */
    private function cancelOrder(int $cartId, string $message = null)
    {
        try {
            $quote = $this->quoteRepository->get($cartId);
        } catch (\Exception $e) {
            return;
        }

        $payment = $quote->getPayment();
        $paymentCodes = [
            DefaultConfigProvider::CODE,
            DefaultConfigProvider::VAULT_CODE,
            ApplePayConfigProvider::CODE,
        ];
        if (in_array($payment->getMethod(), $paymentCodes)) {
            $this->cancelService->cancelOrder($cartId, (string)$quote->getReservedOrderId(), $message);
        }
    }

    /**
     * Cancels an order if an exception occurs during the order creation.
     *
     * @param DefaultCartManagementInterface $subject
     * @param \Closure $proceed
     * @param $cartId
     * @param PaymentInterface|null $payment
     * @return mixed
     * @throws \Exception
     */
    public function aroundPlaceOrder(
        DefaultCartManagementInterface $subject,
        \Closure $proceed,
        $cartId,
        PaymentInterface $payment = null
    ) {
        try {
            return $proceed($cartId, $payment);
        } catch (\Exception $e) {
            $this->cancelOrder((int)$cartId);
            throw $e;
        }
    }
}
