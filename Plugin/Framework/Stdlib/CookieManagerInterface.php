<?php
/**
 * Copyright © Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Plugin\Framework\Stdlib;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Stdlib\CookieManagerInterface as DefaultCookieManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Header;
use Magento\Framework\Stdlib\Cookie\PhpCookieManager;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Store\Model\ScopeInterface;
use Veriteworks\CookieFix\Validator\SameSite;

/**
 * Class CookieManagerInterface
 * @package TotalProcessing\Opp\Plugin\Framework\Stdlib
 */
class CookieManagerInterface
{
    const XML_PATH_COOKIE_CONFIG = 'web/cookie/samesite';

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var SameSite
     */
    private $validator;

    /**
     * @var Header
     */
    private $header;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var string|null
     */
    private $affectedKey;

    /**
     * @param RequestInterface $request
     * @param Header $header
     * @param SameSite $validator
     * @param ScopeConfigInterface $scopeConfig
     * @param string|null $affectedKey
     */
    public function __construct(
        RequestInterface $request,
        Header $header,
        SameSite $validator,
        ScopeConfigInterface $scopeConfig,
        string $affectedKey = null
    ) {
        $this->request = $request;
        $this->header = $header;
        $this->validator = $validator;
        $this->scopeConfig = $scopeConfig;
        $this->affectedKey = $affectedKey;
    }

    /**
     * @param PhpCookieManager $subject
     * @param string $name
     * @param string $value
     * @param PublicCookieMetadata|null $metadata
     * @return array
     */
    public function beforeSetPublicCookie(
        DefaultCookieManagerInterface $subject,
        string $name,
        string $value,
        PublicCookieMetadata $metadata = null
    ): ?array {
        $modified = false;
        if ($name === $this->affectedKey) {
            $agent = (string)$this->header->getHttpUserAgent();
            $sameSite = (bool)$this->validator->shouldSendSameSiteNone($agent);

            if ($sameSite === false) {
                $metadata->setSecure(true)
                    ->setSameSite('None');
            } else {
                $config = $this->scopeConfig->getValue(self::XML_PATH_COOKIE_CONFIG, ScopeInterface::SCOPE_STORE);
                if (strtolower($config) === 'none') {
                    $metadata->setSecure(true);
                }
                $metadata->setSameSite($config);
                // force none for admin
                $metadata->setSecure(true);
                $metadata->setSameSite('None');
            }
            $modified = true;
        }

        return $modified ? [$name, $value, $metadata] : null;
    }
}
