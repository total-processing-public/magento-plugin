<?php
/**
 * Copyright © Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Plugin\Sales\Controller\Adminhtml\Order\Create;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;
use Magento\Framework\Message\AbstractMessage;
use Magento\Framework\Message\ManagerInterface as MessageManagerInterface;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Sales\Controller\Adminhtml\Order\Create\Save as DefaultSave;
use TotalProcessing\Opp\Model\Ui\ApplePay\ConfigProvider as ApplePayConfigProvider;
use TotalProcessing\Opp\Model\Ui\ConfigProvider as DefaultConfigProvider;
use Magento\Backend\Model\Session\Quote as SessionQuote;
use TotalProcessing\Opp\Model\CancelServiceInterface;

/**
 * Class OrderCancellation
 * @package TotalProcessing\Opp\Plugin
 */
class Save
{
    /**
     * @var SessionQuote
     */
    private $sessionQuote;

    /**
     * @var CancelServiceInterface
     */
    private $cancelService;

    /**
     * @var MessageManagerInterface
     */
    private $messageManager;

    /**
     * @param SessionQuote $sessionQuote
     * @param CancelServiceInterface $cancelService
     * @param MessageManagerInterface $messageManager
     */
    public function __construct(
        SessionQuote $sessionQuote,
        CancelServiceInterface $cancelService,
        MessageManagerInterface $messageManager
    ) {
        $this->sessionQuote = $sessionQuote;
        $this->cancelService = $cancelService;
        $this->messageManager = $messageManager;
    }

    /**
     * @return string
     */
    private function prepareMessages(): string
    {
        $messages = [];
        foreach ($this->messageManager->getMessages()->getItems() as $message) {
            /** @var AbstractMessage $message */
            $messages[] = $message->getText();
        }
        return implode("\n", $messages);
    }

    /**
     * @return void
     * @throws LocalizedException
     * @throws NotFoundException
     * @throws CommandException
     */
    private function cancelPayment(): void
    {
        $quote = $this->sessionQuote->getQuote();
        $payment = $quote->getPayment();
        $paymentCodes = [
            DefaultConfigProvider::CODE,
            DefaultConfigProvider::VAULT_CODE,
            ApplePayConfigProvider::CODE,
        ];
        if (in_array($payment->getMethod(), $paymentCodes)) {
            $this->cancelService->cancelPayment($quote, null, [], $this->prepareMessages());
        }
    }

    /**
     * @param DefaultSave $subject
     * @param $result
     * @return mixed
     * @throws CommandException
     * @throws LocalizedException
     * @throws NotFoundException
     */
    public function afterExecute(
        DefaultSave $subject,
        $result
    ) {
        if ($this->sessionQuote->getQuoteId()) {
            // if the storage quote ID still exists, then the order was placed unsuccessfully
            $this->cancelPayment();
        }
        return $result;
    }
}
