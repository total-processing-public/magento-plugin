<?php
/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace TotalProcessing\Opp\Plugin\Backend\App;

use Magento\Backend\App\AbstractAction as DefaultAbstractAction;
use Magento\Backend\Model\Auth;
use Magento\Backend\Model\UrlInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Route\ConfigInterface;
use Magento\Framework\FlagManager;
use Magento\Framework\HTTP\Header;
use TotalProcessing\Opp\Gateway\Config\Config;
use TotalProcessing\Opp\Gateway\Helper\Backend\FlagProvider;

/**
 * Class AbstractAction
 * @package TotalProcessing\Opp\Plugin\Backend\App
 */
class AbstractAction
{
    /**
     * @var Auth
     */
    private $auth;

    /**
     * @var ConfigInterface
     */
    private $routeConfig;

    /**
     * @var FlagManager
     */
    private $flagManager;

    /**
     * @var Header
     */
    private $header;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var UrlInterface
     */
    private $backendUrl;

    /**
     * @var string
     */
    private $gatewayRedirectFullActionName;

    /**
     * @param Auth $auth
     * @param ConfigInterface $routeConfig
     * @param FlagManager $flagManager
     * @param Header $header
     * @param Config $config
     * @param UrlInterface $backendUrl
     * @param string $gatewayRedirectFullActionName
     */
    public function __construct(
        Auth $auth,
        ConfigInterface $routeConfig,
        FlagManager $flagManager,
        Header $header,
        Config $config,
        UrlInterface $backendUrl,
        string $gatewayRedirectFullActionName
    ) {
        $this->auth = $auth;
        $this->routeConfig = $routeConfig;
        $this->flagManager = $flagManager;
        $this->header = $header;
        $this->config = $config;
        $this->backendUrl = $backendUrl;
        $this->gatewayRedirectFullActionName = $gatewayRedirectFullActionName;
    }

    /**
     * @param RequestInterface $request
     * @param string $controllerName
     * @param string $actionName
     * @param bool $forwarded
     * @param bool $dispatched
     * @return void
     */
    private function modifyRequest(
        RequestInterface $request,
        string $controllerName,
        string $actionName,
        bool $forwarded = true,
        bool $dispatched = false
    ) {
        $routeName = $this->routeConfig->getRouteFrontName('totalprocessing_opp');
        $request->setForwarded($forwarded)
            ->setRouteName($routeName)
            ->setControllerName($controllerName)
            ->setActionName($actionName)
            ->setDispatched($dispatched);
    }

    /**
     * Since after the response from the payment gateway the current session can be terminated
     * and then, after the prolongation @see \TotalProcessing\Opp\Plugin\Framework\Stdlib\CookieManagerInterface
     * redirect to the default startup page, we need to modify request to check the payment status.
     * Make sure that the request is from the payment gateway.
     *
     * @param RequestInterface $request
     * @return bool
     */
    private function isNeedModify(RequestInterface $request): bool
    {
        $fullActionName = (string)$request->getFullActionName();
        $startupPageData = str_replace('/', '_', $this->backendUrl->getStartupPageUrl());

        return $this->auth->isLoggedIn()
            && (strpos($fullActionName, $startupPageData) !== false)
            && (rtrim($this->header->getHttpReferer(), '/') === rtrim($this->config->getApiUrl(), '/'));
    }

    /**
     * @param DefaultAbstractAction $subject
     * @param RequestInterface $request
     * @return RequestInterface[]|null
     */
    public function beforeDispatch(
        DefaultAbstractAction $subject,
        RequestInterface $request
    ) {
        $modified = false;

        // Execution of the redirect from payment gateway response.
        // @see \TotalProcessing\Opp\ViewModel\Backend\Order\Create\Payment\IframeViewModel::getRedirectUrl()
        if ((string)$request->getFullActionName() === $this->gatewayRedirectFullActionName) {
            // case if session is terminated
            $redirectSessionId = (string)$request->getParam(FlagProvider::REDIRECT_SESSION_ID);
            if ($redirectSessionId && !$this->auth->isLoggedIn()) {
                // In some reason the backend session is terminated when the redirect URL returned by the payment
                // gateway is executed. Modify request to transparent redirect to prevent backend session termination.
                $this->modifyRequest($request, 'transparent', 'redirect');
                // Save the flag for subsequent processing, in order to notice that the request has been modified.
                $this->flagManager->saveFlag(FlagProvider::REDIRECT_SESSION_ID, $redirectSessionId);
                // Save the flag for subsequent processing, in order to payment failed notification
                $this->flagManager->saveFlag(
                    FlagProvider::REDIRECT_QUOTE_ID,
                    (string)$request->getParam(FlagProvider::REDIRECT_QUOTE_ID)
                );
                $modified = true;
            } elseif ($this->auth->isLoggedIn()) {
                // If the user is already logged in - redirection to check the payment status
                $this->modifyRequest($request, 'payment', 'status');
                $modified = true;
            }
        } else if ($this->isNeedModify($request)) {
            // case if session is prolonging
            $this->modifyRequest($request, 'payment', 'status');
            $modified = true;
        }

        if ($modified) {
            return [$request];
        }

        $redirectSessionIdFlag = (string)$this->flagManager->getFlagData(FlagProvider::REDIRECT_SESSION_ID);
        if (
            $this->auth->isLoggedIn()
            && $redirectSessionIdFlag
            && ((string)$this->auth->getAuthStorage()->getSessionId() === $redirectSessionIdFlag)
        ) {
            // If the user is already logged in and there is a flag about the previous request modify
            // and the flag data match with the current session we need to modify the request
            // and redirect to check the payment status.
            $this->modifyRequest($request, 'payment', 'status');
            // The flag must be removed to prevent any re-directs or unexpected behavior.
            $this->flagManager->deleteFlag(FlagProvider::REDIRECT_SESSION_ID);
            return [$request];
        }

        return null;
    }
}
