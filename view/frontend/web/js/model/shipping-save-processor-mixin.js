/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'mage/utils/wrapper'
], function (wrapper) {
    'use strict';

    return function (shippingSaveProcessor) {
        shippingSaveProcessor.saveShippingInformation = wrapper.wrapSuper(
            shippingSaveProcessor.saveShippingInformation,
            function (type) {
                let updateIframeCallback;

                /**
                 * Update (re-init) payment form to pickup current quote prices
                 */
                updateIframeCallback = function () {
                    let iframe = document.getElementById("totalprocessing_opp_iframe");

                    if (iframe) {
                        let iframeDoc = iframe.contentDocument || iframe.contentWindow.document;
                        iframeDoc.location.reload();
                    }
                };

                return this._super(type).done(updateIframeCallback);
            }
        );

        return shippingSaveProcessor;
    };
});
