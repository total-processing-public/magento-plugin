/**
 * Copyright © Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */

/**
 * @api
 */
define(
    [
        'mage/url',
        'Magento_Checkout/js/model/full-screen-loader'
    ],
    function (url, fullScreenLoader) {
        'use strict';

        return {
            redirectUrl: 'opp/webhook_order/cancel',

            /**
             * Provide redirect to page
             */
            execute: function () {
                fullScreenLoader.startLoader();
                this.redirectToCancelPage();
            },

            /**
             * Redirect to cancel page.
             */
            redirectToCancelPage: function () {
                window.location.replace(url.build(this.redirectUrl));
            }
        };
    }
);