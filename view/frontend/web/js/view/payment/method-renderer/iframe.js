/**
 * Copyright © Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        'jquery',
        'ko',
        'underscore',
        'Magento_Checkout/js/view/payment/default',
        'TotalProcessing_Opp/js/model/iframe',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_CheckoutAgreements/js/view/checkout-agreements',
        'Magento_Customer/js/customer-data',
        'Magento_Checkout/js/action/redirect-on-success',
        'TotalProcessing_Opp/js/action/redirect-on-cancel'
    ],
    function (
        $,
        ko,
        _,
        Component,
        iframeService,
        fullScreenLoader,
        quote,
        additionalValidators,
        checkoutAgreements,
        customerData,
        redirectOnSuccessAction,
        redirectOnCancelAction
    ) {
        'use strict';

        let isVisible = ko.observable(false);

        return Component.extend({
            defaults: {
                template: 'TotalProcessing_Opp/payment/iframe',
                active: false,
                isVisible: isVisible,
                code: 'totalprocessing_opp',
                isInAction: iframeService.isInAction,
                isLightboxReady: iframeService.isLightboxReady,
                isLoading: ko.observable(false),
                isPlainFramePlaceholderVisible: ko.observable(false),
                isCardFramePlaceholderVisible: ko.observable(false),
                isPlaceOrderAvailable: ko.observable(true),
                isSuccessCreatePendingOrder: ko.observable(false)
            },

            /**
             * Returning the code of the payment method.
             *
             * @returns {*}
             */
            getCode: function () {
                return this.code;
            },

            /**
             * Returning the style attribute value for the iframe.
             *
             * @returns {string}
             */
            getStyle: function () {
                if (this.isActive()) {
                    return window.checkoutConfig.payment[this.getCode()].iframeStyles;
                }
                return "";
            },

            /**
             * Returning the cc icons
             *
             * @returns {*}
             */
            getIcons: function () {
                let icons = [];

                _.each(window.checkoutConfig.payment[this.getCode()].icons, function (icon) {
                    icons.push(icon);
                })

                return icons;
            },

            /**
             * Checking if the payment method is active.
             *
             * @returns {*|boolean}
             */
            isActive: function () {
                let isActive = this.getCode() === this.isChecked() && this.isCountryAvailable();
                this.processingFramePlaceholder(true);
                return isActive;
            },

            /**
             * Returning the source of the iframe.
             *
             * @returns {*}
             */
            getSource: function () {
                return window.checkoutConfig.payment[this.getCode()].source;
            },

            /**
             * Whether the payment webhooks must be use
             *
             * @return {any}
             */
            isWebhooksAvailable: function () {
                return window.checkoutConfig.payment[this.getCode()].isWebhooksAvailable;
            },

            /**
             * This function is called when the iframe is loaded.
             */
            iframeLoaded: function () {
                fullScreenLoader.stopLoader();
            },

            /**
             * Validate checkout agreements
             * @param contextEvent
             */
            validateCheckoutAgreements: function (contextEvent) {
                let self = this,
                    agreements = checkoutAgreements().agreements,
                    shouldDisableAction = false;

                this.isPlaceOrderAvailable(false);

                if (checkoutAgreements().isVisible) {
                    _.each(agreements, function (item) {
                        if (checkoutAgreements().isAgreementRequired(item)) {
                            let paymentMethodCode = quote.paymentMethod().method,
                                inputId = '#agreement_' + paymentMethodCode + '_' + item.agreementId,
                                inputElement = document.querySelector(inputId);

                            if (!inputElement.checked) {
                                additionalValidators.validate();
                                shouldDisableAction = true;
                            }

                            inputElement.addEventListener('change', function (event) {
                                if (additionalValidators.validate()) {
                                    self.isPlaceOrderAvailable(true);
                                } else {
                                    self.isPlaceOrderAvailable(false);
                                }
                            });
                        }
                    });
                }

                if (!shouldDisableAction) {
                    self.isPlaceOrderAvailable(true);
                }

                contextEvent.valid = self.isPlaceOrderAvailable();
            },

            /**
             * This function is called when the iframe is loaded.
             */
            initEventListenersPlain: function () {
                let self = this;

                window.onbeforeunload = function (e) {
                    self.resizeIframe(0);
                };

                window.document.addEventListener("iframe", function (e) {
                    let iframe = document.getElementById(self.getCode() + "_iframe");

                    iframe.height = e.detail.iframeHeight + "px";
                    self.isVisible(e.detail.isVisible);

                    if (e.detail.additionalValidation) {
                        self.validateCheckoutAgreements(e.detail.contextEvent);
                    }

                    if (e.detail.placeOrder && self.isPlaceOrderAvailable()) {
                        fullScreenLoader.startLoader();
                        self.placeOrder();
                    }

                    if (!self.isVisible()) {
                        self.processingFramePlaceholder(false);
                    }
                });
            },

            /**
             * This function is called when the iframe is loaded.
             */
            initEventListeners: function () {
                let self = this;

                window.onbeforeunload = function (e) {
                    self.resizeIframe(0);
                };

                window.document.addEventListener("iframe", function (e) {
                    let iframe = document.getElementById(self.getCode() + "_iframe");

                    iframe.height = e.detail.iframeHeight + "px";
                    self.isVisible(e.detail.isVisible);

                    if (e.detail.additionalValidation) {
                        self.validateCheckoutAgreements(e.detail.contextEvent);

                        if (self.isPlaceOrderAvailable() && self.isWebhooksAvailable()) {
                            // disallow to execute payment transaction before magento create an order
                            e.detail.contextEvent.valid = false;
                            // disallow to redirect on success page after place order
                            // before the transaction is processed
                            self.redirectAfterPlaceOrder = false;

                            // make sure the order is created
                            if (!self.isSuccessCreatePendingOrder()) {
                                self.placeOrder();
                            }

                            // afterPlaceOrder post-process action
                            if (self.isSuccessCreatePendingOrder()) {
                                // allow to execute payment transaction
                                e.detail.contextEvent.valid = true;
                            }
                        }
                    }

                    if (self.isWebhooksAvailable() && e.detail.placeOrder && self.isSuccessCreatePendingOrder()) {
                        // workaround when mini-cart still shows items after checkout is complete
                        let sections = ['cart'];
                        customerData.invalidate(sections);
                        customerData.reload(sections, true);

                        redirectOnSuccessAction.execute();
                    } else if (!self.isWebhooksAvailable() && e.detail.placeOrder && self.isPlaceOrderAvailable()) {
                        fullScreenLoader.startLoader();
                        self.placeOrder();
                    }

                    if (!self.isVisible()) {
                        self.processingFramePlaceholder(false);
                    }
                });
            },

            /**
             * This function is called when the payment method is selected.
             *
             * @returns {*}
             */
            selectPaymentMethod: function () {
                this.processingFramePlaceholder(true);
                return this._super();
            },

            /**
             * This function is used to resize the iframe.
             *
             * @param height
             */
            resizeIframe: function (height) {
                let iframe = document.getElementById(this.getCode() + "_iframe");

                if ($(iframe).length > 0) {
                    iframe.height = height + "px";
                }
            },

            /**
             * This function is called when the user clicks on the "Place Order" button.
             */
            placePendingPaymentOrder: function () {
                let iframe = document.getElementById(this.getCode() + '_iframe');

                iframe.contentWindow.wpwl.executePayment('wpwl-container-card');
            },

            /**
             * This function is used to get the text of the button.
             * @returns {*}
             */
            getButtonText: function () {
                return window.checkoutConfig.payment[this.getCode()].paymentBtnText;
            },

            /**
             * After place order callback
             */
            afterPlaceOrder: function () {
                this._super();

                if (this.isWebhooksAvailable()) {
                    this.isSuccessCreatePendingOrder(true);
                    this.placePendingPaymentOrder();
                }
            },

            /**
             * This function is called when the user clicks on the "Place Order" button.
             *
             * @returns {*}
             */
            getPlaceOrderDeferredObject: function () {
                let self = this;

                this.processingFramePlaceholder(true);

                return this._super().fail(function () {
                    fullScreenLoader.stopLoader();
                    self.processingFramePlaceholder(false);
                    self.isInAction(false);
                    document.removeEventListener('click', iframeService.stopEventPropagation, true);
                    self.isSuccessCreatePendingOrder(false);

                    setTimeout(function () {
                        if (self.isWebhooksAvailable()) {
                            redirectOnCancelAction.execute();
                        } else {
                            window.location.reload()
                        }
                    }, 5000);
                });
            },

            /**
             * This function is used to check if the country is available for the payment method.
             *
             * @returns {boolean|*}
             */
            isCountryAvailable: function () {
                let country = quote.billingAddress._latestValue.countryId;
                let listed = window.checkoutConfig.payment[this.getCode()].availableCountries;

                if (listed.find(element => element == "All" || element == country)) {
                    // Prev: return this.isRadioButtonVisible();
                    // Can't use the previous one because it leads to incorrect behavior under certain conditions.
                    // It's incorrect to bind to the value whether the radio button is available or not,
                    // because if only one payment method is enabled, this value will be returned false,
                    // the radio button will be already checked and hidden by default, since there is no point in
                    // displaying it when only one payment method is available. Need to literally specify the correct
                    // return result without reference to other variables.
                    return true;
                }

                return false;
            },

            /**
             * This function is used to show the loading spinner when the iframe is loading.
             *
             * @param visibilityFlag
             */
            processingFramePlaceholder: function (visibilityFlag) {
                let config = window.checkoutConfig.payment[this.getCode()], styleOptions;

                if (config.hasOwnProperty('styleOptions')) {
                    styleOptions = config.styleOptions;
                }

                if (styleOptions == 'plain') {
                    this.isPlainFramePlaceholderVisible(visibilityFlag);
                }
                if (styleOptions == 'card') {
                    this.isCardFramePlaceholderVisible(visibilityFlag);
                }
            }
        });
    }
)
