/**
 * Copyright © Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
define(
    [
        "jquery",
        'ko',
        'underscore',
        'Magento_Checkout/js/view/payment/default',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/model/quote',
        'Magento_Ui/js/model/messageList',
        'TotalProcessing_Opp/js/service/payment/applepay',
        'mage/translate',
        'Magento_Checkout/js/model/payment/additional-validators',
        'Magento_CheckoutAgreements/js/view/checkout-agreements',
        'Magento_Checkout/js/action/redirect-on-success'
    ],
    function (
        $,
        ko,
        _,
        Component,
        fullScreenLoader,
        quote,
        messageList,
        applePay,
        $t,
        additionalValidators,
        checkoutAgreements,
        redirectOnSuccessAction
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                active: false,
                buttonActive: true,
                code: 'totalprocessing_opp_applepay',
                template: 'TotalProcessing_Opp/payment/applepay',
                token: null,
                isActionAvailable: ko.observable(true)
            },

            /**
             * Function that is called when the component is initialized.
             *
             * @returns {*}
             */
            initObservable: function () {
                let self = this;

                self._super().observe(['active', 'buttonActive']);

                return this;
            },

            /**
             * Adding an error message to the message list.
             *
             * @param message
             */
            addErrorMessage: function (message) {
                messageList.addErrorMessage({
                    message: message
                });
            },

            /**
             * Validate checkout agreements
             */
            validateCheckoutAgreements: function () {
                let self = this,
                    agreements = checkoutAgreements().agreements,
                    shouldDisableAction = false;

                this.isActionAvailable(false);

                if (checkoutAgreements().isVisible) {
                    _.each(agreements, function (item) {
                        if (checkoutAgreements().isAgreementRequired(item)) {
                            let paymentMethodCode = quote.paymentMethod().method,
                                inputId = '#agreement_' + paymentMethodCode + '_' + item.agreementId,
                                inputElement = document.querySelector(inputId);

                            if (!inputElement.checked) {
                                additionalValidators.validate();
                                shouldDisableAction = true;
                            }

                            inputElement.addEventListener('change', function (event) {
                                if (additionalValidators.validate()) {
                                    self.isActionAvailable(true);
                                } else {
                                    self.isActionAvailable(false);
                                }
                            });
                        }
                    });
                }

                if (!shouldDisableAction) {
                    self.isActionAvailable(true);
                }
            },

            /**
             * @param message
             * @returns {boolean}
             */
            sendPaymentFailedNotification: function (message) {
                $.ajax({
                    url: window.checkoutConfig.payment[this.getCode()].paymentFailedNotificationUrl,
                    type: 'POST',
                    context: this,
                    data: {
                        message: message,
                    },
                    dataType: 'JSON'
                });
                return false;
            },

            /**
             * This is a function that is called when the user clicks the place order button.
             */
            beforePlaceOrder: function () {
                let self = this;

                self.buttonActive(false);

                if (!quote.billingAddress()) {
                    self.addErrorMessage($t("Billing address is required"));
                    self.buttonActive(true);
                    return;
                }

                self.validateCheckoutAgreements();

                if (!self.isActionAvailable()) {
                    self.buttonActive(true);
                    return;
                }

                let data = {
                    total: {
                        label: this.getDisplayName(),
                        amount: quote.totals()['base_grand_total']
                    },
                    currencyCode: quote.totals()['base_currency_code'],
                    supportedNetworks: this.getAllowedBrandTypes(),
                    countryCode: quote.billingAddress().countryId
                };

                applePay.process(data, self);
            },

            /**
             * Place order.
             */
            placeOrder: function (data, event) {
                let self = this;

                if (event) {
                    event.preventDefault();
                }

                if (this.validate() &&
                    additionalValidators.validate() &&
                    this.isPlaceOrderActionAllowed() === true
                ) {
                    this.isPlaceOrderActionAllowed(false);

                    this.getPlaceOrderDeferredObject()
                        .done(
                            function () {
                                self.afterPlaceOrder();

                                if (self.redirectAfterPlaceOrder) {
                                    redirectOnSuccessAction.execute();
                                }
                            }
                        ).fail(
                            function () {
                                self.messageContainer.clear();
                                self.messageContainer.addErrorMessage({
                                    message: $t('There was an issue with your payment, please try again or pay by card.')
                                })
                                self.buttonActive(true);
                            }
                        ).always(
                            function () {
                                self.isPlaceOrderActionAllowed(true);
                                self.buttonActive(true);
                            }
                        );

                    return true;
                }

                return false;
            },

            /**
             * Getting the allowed brand types from the config.
             *
             * @returns {*}
             */
            getAllowedBrandTypes: function () {
                return window.checkoutConfig.payment[this.getCode()].availableBrandTypes;
            },

            /**
             * Getting the button text from the config.
             *
             * @returns {*}
             */
            getButtonText: function () {
                return window.checkoutConfig.payment[this.getCode()].paymentBtnText;
            },

            /**
             * Returning the code of the payment method.
             *
             * @returns {*}
             */
            getCode: function () {
                return this.code;
            },

            /**
             * Get the URL that the Apple Pay JS SDK will call to validate the merchant.
             *
             * @returns {*}
             */
            getCompleteMerchantValidationUrl: function () {
                return window.checkoutConfig.payment[this.getCode()].completeMerchantValidationUrl;
            },

            /**
             * Returning the data that will be sent to the server.
             *
             * @returns {{additional_data: {token}, method: *}}
             */
            getData: function () {
                return {
                    'method': this.getCode(),
                    'additional_data': {
                        'token': this.token
                    }
                };
            },

            /**
             * Getting the display name from the config.
             *
             * @returns {*}
             */
            getDisplayName: function () {
                return window.checkoutConfig.payment[this.getCode()].displayName;
            },

            /**
             * Getting the merchant id from the config.
             *
             * @returns {null|*}
             */
            getMerchantId: function () {
                return window.checkoutConfig.payment[this.getCode()].merchantId;
            },

            /**
             * Getting the payment icon from the config.
             *
             * @returns {*}
             */
            getPaymentIcon: function () {
                return window.checkoutConfig.payment[this.getCode()].icon;
            },

            /**
             * Checking if the payment method is active.
             *
             * @returns {boolean}
             */
            isActive: function () {
                let active = this.getCode() === this.isChecked();
                this.active(active);
                return active;
            },

            /**
             * Checking if the payment method is active and if the button is active.
             *
             * @returns {false|boolean}
             */
            isButtonActive: function () {
                return this.isActive() && this.buttonActive;
            },

            /**
             * Setting the token that is returned from the Apple Pay JS SDK.
             *
             * @param token
             */
            setToken: function (token) {
                this.token = token;
            }
        });
    }
)
