/**
 * Copyright Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    config: {
        mixins: {
            'Magento_Checkout/js/model/shipping-save-processor': {
                'TotalProcessing_Opp/js/model/shipping-save-processor-mixin': true
            },
        }
    }
};
