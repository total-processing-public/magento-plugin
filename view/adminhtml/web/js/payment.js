/**
 * Copyright © Total Processing. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define([
    'jquery',
    'uiComponent',
    'Magento_Ui/js/modal/alert',
    'Magento_Ui/js/lib/view/utils/dom-observer',
    'mage/translate',
    'mage/cookies'
], function ($, Class, alert, domObserver, $t) {
    'use strict';

    return Class.extend({

        defaults: {
            $body: $('body'),
            $selector: null,
            selector: 'edit_form',
            submitOrderButton: '#submit_order_top_button, #edit_form .actions .save.primary',
            iframeContainer: 'totalprocessing_opp_iframe',
            iframeWrapper: 'order-opp_payment_iframe_source',
            iframeMessages: 'order-opp_payment_iframe_messages',
            iframeLoaded: false,
            iframe: null,
            active: false,
            isBeforeSubmitValid: true,
            imports: {
                onActiveChange: 'active'
            }
        },

        /** @inheritdoc */
        initialize: function () {
            let self = this;

            self._super();

            // listen iframe
            window.document.addEventListener("iframe", function (event) {
                let iframe = document.getElementById(self.iframeContainer);

                iframe.height = event.detail.iframeHeight + "px";

                if (event.detail.editFormValidation) {
                    // validate parent form
                    self.validateEditForm();
                    if (self.$selector.validate().errorList.length && event.detail.contextEvent) {
                        event.detail.contextEvent.valid = false;
                        event.detail.contextEvent.preventDefault();
                        return;
                    }

                    // apply validator rules
                    self.validateBeforeSubmit();
                    if (!self.isBeforeSubmitValid) {
                        event.detail.contextEvent.valid = false;
                        event.detail.contextEvent.preventDefault();
                        return;
                    }

                    event.detail.contextEvent.valid = true;
                    self.updateSubmitOrderBtnState(true);
                    self.isBeforeSubmitValid = true;
                    return;
                }

                if (event.detail.placeOrder) {
                    self.$body.trigger('processStart');
                    self.$selector.trigger('realOrder');
                    return;
                }

                self.$body.trigger('processStop');
            });
        },

        /**
         * Set list of observable attributes
         * @returns {exports.initObservable}
         */
        initObservable: function () {
            let self = this,
                paymentSelector = '[name="payment[method]"][value="' + this.getCode() + '"]:checked';

            self.$selector = $('#' + self.selector);
            this._super()
                .observe([
                    'active',
                    'iframeLoaded',
                ]);

            if (self.$selector.find(paymentSelector).length !== 0) {
                this.active(true);
            }

            // re-init payment method events
            self.$selector.off('changePaymentMethod.' + this.getCode())
                .on('changePaymentMethod.' + this.getCode(), this.changePaymentMethod.bind(this));

            // listen iframe wrapper (due to layout areas update (cart items, shipping methods, etc.))
            domObserver.get('#' + self.iframeWrapper, function () {
                if (!$('#' + self.iframeContainer).length) {
                    self.iframe = null;
                    self.iframeLoaded(false);

                    if (self.$selector.find(paymentSelector).length !== 0) {
                        self.initIframe();
                    }
                }
            });

            // listen iframe state
            domObserver.get('#' + self.iframeContainer, function () {
                if (self.active()) {
                    self.disableEventListeners();
                    self.enableEventListeners();
                }
            });

            return this;
        },

        /**
         * Remove payment iframe widget from layout
         */
        destroyPaymentWidget: function () {
            let $iframe = $('#' + this.iframeContainer);

            if (!$iframe.length) {
                this.iframe = null;
                this.iframeLoaded(false);
                return;
            }

            let wpwlObject = $iframe[0].contentWindow.wpwl;

            if (wpwlObject !== undefined && wpwlObject.unload !== undefined) {
                wpwlObject.unload();
                $iframe.contents().find('script').each(function () {
                    if (this.src.indexOf('static.min.js') !== -1) {
                        $(this).remove();
                    }
                });
            }

            $iframe.remove();
            this.iframe = null;
            this.iframeLoaded(false);
        },

        /**
         * Enable/disable current payment method
         * @param {Object} event
         * @param {String} method
         * @returns {exports.changePaymentMethod}
         */
        changePaymentMethod: function (event, method) {
            this.active(method === this.code);
            return this;
        },

        /**
         * Triggered when payment changed
         * @param {Boolean} isActive
         */
        onActiveChange: function (isActive) {
            if (!isActive) {
                this.$selector.off('submitOrder.' + this.getCode());
                this.destroyPaymentWidget();
                this.updateSubmitOrderBtnState();
                return;
            }

            this.disableEventListeners();

            if (typeof window.order !== 'undefined') {
                window.order.addExcludedPaymentMethod(this.getCode());
            }

            if (!this.iframeLoaded()) {
                this.initIframe();
            }

            this.enableEventListeners();
        },

        /**
         * Setup payment
         */
        initIframe: function () {
            let self = this;
            this.iframeLoaded(true);

            self.disableEventListeners();

            try {
                self.$body.trigger('processStart');

                if (!this.iframe) {
                    this.iframe = $('<iframe></iframe>', {
                        id: this.getCode() + '_iframe',
                        name: this.getCode() + '_iframe',
                        style: this.iframeStyles,
                        src: this.renderSrc
                    })
                }

                let $wrapper = $('#' + this.iframeWrapper);
                $wrapper.parent().addClass('opp-payment-wrapper');
                this.iframe.appendTo($wrapper);
            } catch (e) {
                self.$body.trigger('processStop');
                self.error(e.message);
                console.log(e);
            }
        },

        /**
         * @return {boolean}
         */
        validateBeforeSubmit: function () {
            let self = this,
                $messageContainer = $('#' + self.iframeMessages),
                $textContainer = $messageContainer.find('[data-role="text"]'),
                data = {},
                formKey = window.FORM_KEY || $.mage.cookies.get('form_key');

            $.extend(data, {
                'form_key': formKey
            });

            $.ajax({
                url: this.validationUrl,
                type: 'POST',
                data: data,
                async: false,
                cache: false,

                /** @inheritdoc */
                beforeSend: function () {
                    self.isBeforeSubmitValid = true;
                    $messageContainer.hide();
                    $textContainer.html('').trigger('contentUpdated');
                },

                /** @inheritdoc */
                complete: function () {
                    self.$body.trigger('processStop');
                }
            }).done(function (response) {
                if (response.errorMessage) {
                    console.log('Payment validation error: ' + response.errorMessage);
                }

                let validationResult = response.validationResult;
                if ((validationResult.isValid == false) && validationResult.messages) {
                    $textContainer.html(validationResult.messages).trigger('contentUpdated');
                    $messageContainer.show();
                    self.isBeforeSubmitValid = false;
                }
            }).fail(function (error) {
                $textContainer.html(error).trigger('contentUpdated');
                $messageContainer.show();
                self.isBeforeSubmitValid = false;
                console.log('Payment validation error: ' + JSON.stringify(error));
            }).always(function () {
                self.$body.trigger('processStop');
            });

            if (this.isBeforeSubmitValid) {
                $messageContainer.hide();
                $textContainer.html('').trigger('contentUpdated');
            }
        },

        /**
         * Validate the order create form
         */
        validateEditForm: function () {
            this.$selector.validate().form();
            this.$selector.trigger('afterValidate.beforeSubmit');
            this.$body.trigger('processStop');
        },

        /**
         * Disable form event listeners
         */
        disableEventListeners: function () {
            this.$selector.off('submitOrder');
            this.$selector.off('submit');
        },

        /**
         * Enable form event listeners
         */
        enableEventListeners: function () {
            this.$selector.on('submitOrder.' + this.getCode(), this.submitOrder.bind(this));
        },

        /**
         * Trigger order submit
         *
         * @returns {boolean}
         */
        submitOrder: function (event) {
            // validate parent form
            this.validateEditForm();
            if (this.$selector.validate().errorList.length) {
                return false;
            }

            // validate iframe form
            if (!this.isIframeValid()) {
                event.preventDefault();
                event.stopImmediatePropagation();
                return false;
            }
        },

        /**
         * @returns {boolean}
         */
        isIframeValid: function () {
            let self = this,
                $iframe = $('#' + this.iframeContainer),
                $payBtn = this.getPayBtn();

            if (!$iframe.length || !$payBtn) {
                this.error($t('Payment form is not detected. Please refresh the page.'))
                return false;
            }

            let wpwl = $iframe[0].contentWindow.wpwl;
            if (wpwl !== undefined) {
                wpwl.executePayment('wpwl-container-card');

                setTimeout(function () {
                    // @TODO - is there another way to check if the payment iframe is valid?
                    if ($payBtn.hasClass('wpwl-button-error')) {
                        self.iframeFocus();
                        self.error($t('Please fill in all required fields in the payment form.'))
                        return false;
                    }
                    self.updateSubmitOrderBtnState(true);
                    return true;
                },100)
            }

            return false;
        },

        /**
         * Set viewport iframe focus
         */
        iframeFocus: function () {
            let $iframe = $('#' + this.iframeContainer);

            $('html, body').animate({
                scrollTop: $iframe.offset().top - 150
            }, 800);
            $iframe.focus();
        },

        /**
         * @param flag
         */
        updateSubmitOrderBtnState: function (flag)
        {
            let $btn = $(this.submitOrderButton);

            if (!$btn.length) {
                return;
            }

            if (flag) {
                $btn.prop('disabled', true);
                return;
            }
            $btn.prop('disabled', false);
        },

        /**
         * Place order
         */
        placeOrder: function () {
            $('#' + this.selector).trigger('realOrder');
        },

        /**
         * Get iframe pay button
         *
         * @returns {*|boolean}
         */
        getPayBtn: function () {
            let $iframe = $('#' + this.iframeContainer);

            if (!$iframe.length) {
                return false;
            }

            return $iframe.contents().find('.wpwl-button-pay');
        },

        /**
         * Get payment method code
         *
         * @returns {String}
         */
        getCode: function () {
            return this.code;
        },

        /**
         * Show alert message
         *
         * @param {String} message
         */
        error: function (message) {
            alert({
                content: message
            });
        },

        /**
         * Processing response errors
         *
         * @param {Object} response
         * @private
         */
        processErrors: function (response) {
            let message = response['error_messages'];

            if (typeof message === 'object') {
                alert({
                    content: message.join('\n')
                });
            }

            if (message) {
                this.error(message)
            }
        }
    });
});
