<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TotalProcessing\Opp\Api;

use TotalProcessing\Opp\Exception\WebhookException;
use Magento\Framework\Exception\LocalizedException;

/**
 * * Webhook order managements interface.
 *
 * Interface WebhookOrderProcessorInterface
 * @package TotalProcessing\Opp\Api
 */
interface WebhookOrderProcessorInterface
{
    /**
     * Update order information
     *
     * @param mixed $paymentData
     * @param bool $delayedUpdate
     * @param int|null $quoteId
     * @return void
     * @throws WebhookException
     * @throws LocalizedException
     * @throws \Exception
     */
    public function update($paymentData = null, bool $delayedUpdate = true, int $quoteId = null): void;
}